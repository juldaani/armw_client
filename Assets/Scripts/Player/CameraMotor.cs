﻿using UnityEngine;
using System.Collections;

public class CameraMotor : MonoBehaviour {

    private Transform lookAt;

	// Use this for initialization
	void Start () {
        lookAt = GameObject.FindGameObjectWithTag("DeviceLocation").transform;
	}
	
	// Update is called once per frame
	void Update () {

        transform.position = lookAt.position;
        transform.rotation = lookAt.rotation;
	}
}
