﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour {

    // How far is the player allowed to move from the mobile device's location (based on gps) on the map.
    public int maxDistFromDeviceLocation;
    public float speed;
    public float slowdownDistance;

    GameObject deviceLocation;


    void Start()
    {
        deviceLocation = GameObject.FindGameObjectWithTag("DeviceLocation");
        transform.position = deviceLocation.transform.position;
    }


    // Draw debug circle for the slowdown distance
    void OnDrawGizmos()
    {
        if (deviceLocation != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(deviceLocation.transform.position, 300);//maxDistFromDeviceLocation);
            Gizmos.DrawWireSphere(deviceLocation.transform.position, maxDistFromDeviceLocation);
        }
    }


    // Update is called once per frame
    void FixedUpdate () {

        // Get data from acceleration sensors
        float accX = Input.acceleration.x;
        float accY = Input.acceleration.y;

        // Distance between device location (based on gps) and player
        float dist = Vector3.Distance(deviceLocation.transform.position, transform.position);

        Vector3 moveTo = new Vector3(accX * speed, 0, accY * speed);

        // Indicates if player is moving farther from the deviceLocation
        bool isMovingAway = Vector3.Magnitude((transform.position - deviceLocation.transform.position) + moveTo) > dist;

        // Slow down player's velocity if we are approaching maxDistFromDeviceLocation
        if ( (dist > maxDistFromDeviceLocation - slowdownDistance) && (isMovingAway == true) )
        {
            accX =  (maxDistFromDeviceLocation - dist) / slowdownDistance * accX;
            accY =  (maxDistFromDeviceLocation - dist) / slowdownDistance * accY;

            moveTo = new Vector3(accX * speed, 0, accY * speed);
        }

        // If outside of maxDistFromDeviceLocation
        if (dist > maxDistFromDeviceLocation)
        {
            moveTo = (transform.position - deviceLocation.transform.position) * -0.01f;
        }

        // Move player
        transform.Translate(moveTo);

    }
}
