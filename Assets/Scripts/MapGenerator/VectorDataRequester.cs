﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



// Class to be serialized
public class JsonParams{
	public int offset;
	public double lon;
	public double lat;
	public List<string> tags;
}

public class VectorDataRequester : MonoBehaviour {

    private WWW geoJsonData;
    private bool isDownloadFinished;

    // Post json request to the server. 
    public WWW SendRequestToServer(float longitude, float latitude, int offset,
        List<string> tags, string vectorApiUrl)
    {
        isDownloadFinished = false;

        WWWForm form = new WWWForm();

		// Create the parameter object for the Json-request
		JsonParams requestParams = new JsonParams();
		requestParams.lon = longitude;
		requestParams.lat = latitude;
		requestParams.offset = offset;
		requestParams.tags = tags;

		// Convert to JSON (and to bytes)
		string jsonData = JsonUtility.ToJson(requestParams);
		byte[] postData = System.Text.Encoding.ASCII.GetBytes(jsonData);

		Dictionary<string, string> postHeader = form.headers;
		if (postHeader.ContainsKey("Content-Type"))
			postHeader["Content-Type"] = "application/json";
		else
			postHeader.Add("Content-Type", "application/json");
		WWW wwwData = new WWW(vectorApiUrl, postData, postHeader);
		StartCoroutine(WaitForRequest(wwwData));
		return wwwData;
	}


	private IEnumerator WaitForRequest(WWW data)
	{
		yield return data; // Wait until the download is done
        if (data.error != null)
        {
            Debug.Log("There was an error sending request: " + data.error);
        }
        else
        {
            //geometryObjects = new VectorGeometries(data.text);
            geoJsonData = data;
            isDownloadFinished = true;
            Debug.Log("Datan lataus OK");
        }
	}


    // Getters & setters
    public WWW GetGeoJsonData()
    {
        WWW tmp = geoJsonData;
        geoJsonData = null;     // To prevent using old data when the map is updated
        return tmp;
    }

    public bool IsDownloadFinsished()
    {
        return isDownloadFinished;
    }

}





