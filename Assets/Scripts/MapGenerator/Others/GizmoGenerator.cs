﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GizmoGenerator : MonoBehaviour {

    // For testing
    void OnDrawGizmos()
    {
        MapGeneratorManager mapGeneratorManager = gameObject.GetComponent<MapGeneratorManager>();
        VectorGeometries geometries = mapGeneratorManager.GetGeometries();

        if (geometries != null)
        {
            Dictionary<string, GeometryCollection> geomDict = geometries.GetGeometryCollections();
            
            try
            {
                GeometryCollection geomCollection = geomDict["building"];
                List<Polygon> polygons = geomCollection.GetPolygons();
                draw(polygons, Color.yellow);
            }
            catch (System.Exception e) { }

            try
            {
                GeometryCollection geomCollection2 = geomDict["highway"];
                List<LineString> lineStrings = geomCollection2.GetLineStrings();
                draw(lineStrings, Color.red);
            }
            catch (System.Exception e) { }
            
            try
            {
                GeometryCollection geomCollection3 = geomDict["landuse"];
                List<Polygon> polygons2 = geomCollection3.GetPolygons();
                //List<Geometry> polygons2 = geometries.GetGeometriesWithTagAndProperty("landuse", "farmland", "Polygon");
                draw(polygons2, Color.green);
            }
            catch (System.Exception e) { }
            /*
            try
            {
                //GeometryCollection geomCollection4 = geomDict["natural"];
                //List<Polygon> polygons3 = geomCollection4.GetPolygons();
                //List<Geometry> polygons3 = geometries.GetGeometriesWithTagAndProperty("natural", "water", "Polygon");
                List<Geometry> polygons3 = geometries.GetGeometriesWithTagAndProperty("waterway", "stream", "Linestring");
                draw(polygons3, Color.blue);
            }
            catch (System.Exception e) { }
            */
            //Gizmos.color = new Color(1, 0, 0, 0.7F);
            //Gizmos.DrawCube(new Vector3(0, 0, 0), new Vector3(10,10,10));

            // Bounding box
            Gizmos.color = Color.blue;
            int height = 200;
            Coord bboxMin = geometries.GetTransformedBbox()["min"];
            Coord bboxMax = geometries.GetTransformedBbox()["max"];
            Gizmos.DrawLine(new Vector3(bboxMin.X, height, bboxMin.Y), new Vector3(bboxMin.X, height, bboxMax.Y));
            Gizmos.DrawLine(new Vector3(bboxMin.X, height, bboxMax.Y), new Vector3(bboxMax.X, height, bboxMax.Y));
            Gizmos.DrawLine(new Vector3(bboxMax.X, height, bboxMax.Y), new Vector3(bboxMax.X, height, bboxMin.Y));
            Gizmos.DrawLine(new Vector3(bboxMax.X, height, bboxMin.Y), new Vector3(bboxMin.X, height, bboxMin.Y));

        }
    }

    private void draw<T>(List<T> geometries, Color color) where T : Geometry
    {
        TerrainGenerator terrainGenerator = GameObject.Find("MapGenerator").GetComponent<TerrainGenerator>();
        int terrainBaseHeight = terrainGenerator.terrainBaseHeight;
        float terrainMaxHeight = terrainGenerator.height;
        Terrain terrain = terrainGenerator.terrain;

        foreach (T geom in geometries)
        {
            List<List<Coord>> coords = geom.getTransformedCoordinates();

            foreach (List<Coord> coordList in coords)
            {
                float xCur = 0, zCur = 0, xPrev = 0, zPrev = 0;
                bool firstRound = true;
                int offset = 2;

                float alpha = 0.0f;

                foreach (Coord coord in coordList)
                {
                    float terrainHeight = terrain.SampleHeight(new Vector3(coord.X, 0, coord.Y));
                    //float terrainHeight = 200f;

                    alpha = Mathf.Min(alpha + 0.10f, 1.0f);
                    Gizmos.color = new Color(1, 0, 0, alpha);
                    Gizmos.DrawCube(new Vector3(coord.X, terrainHeight+ offset, coord.Y), new Vector3(3, 3, 3));

                    if (firstRound)
                    {
                        xPrev = coord.X;
                        zPrev = coord.Y;
                        firstRound = false;
                    }
                    else
                    {
                        xCur = coord.X;
                        zCur = coord.Y;

                        float terrainHeightPrev = terrain.SampleHeight(new Vector3(xPrev, 0, zPrev));
                        //float terrainHeightPrev = 200f;

                        Gizmos.color = color;
                        Gizmos.DrawLine(new Vector3(xCur, terrainHeight+ offset, zCur), new Vector3(xPrev, terrainHeightPrev + offset, zPrev));

                        xPrev = xCur;
                        zPrev = zCur;

                        /*
                        int DIV = 4;
                        float diffX = xCur - xPrev;
                        float diffZ = zCur - zPrev;
                        float incrementX = -1.0f*diffX / DIV;
                        float slope = diffZ / diffX;
                        for(int i=1; i<DIV; i++)
                        {
                            float tmpX = xCur + i * incrementX;
                            float tmpZ = zCur + i * incrementX * slope;

                            Gizmos.color = new Color(1, 0, 0, 0.7F);
                            Gizmos.DrawCube(new Vector3(tmpX, 0, tmpZ), new Vector3(2, 2, 2));
                        }*/

                    }
                }
            }
        }
    }

}
