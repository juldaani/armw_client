﻿using UnityEngine;
using System.Collections;
using System;

public class CoordinateConverter
{

    public static Coord ToGeographic(double webMercatorX, double webMercatorY)
    {
        if (Math.Abs(webMercatorX) < 180 && Math.Abs(webMercatorY) < 90)
            return null;

        if ((Math.Abs(webMercatorX) > 20037508.3427892) || (Math.Abs(webMercatorY) > 20037508.3427892))
            return null;

        double x = webMercatorX;
        double y = webMercatorY;
        double num3 = x / 6378137.0;
        double num4 = num3 * 57.295779513082323;
        double num5 = Math.Floor((double)((num4 + 180.0) / 360.0));
        double num6 = num4 - (num5 * 360.0);
        double num7 = 1.5707963267948966 - (2.0 * Math.Atan(Math.Exp((-1.0 * y) / 6378137.0)));
        double lon = num6;
        double lat = num7 * 57.295779513082323;

        return new Coord((float)lon, (float)lat);
    }

    public static Coord ToWebMercator(double lon, double lat)
    {
        if ((Math.Abs(lon) > 180 || Math.Abs(lat) > 90))
            return null;

        double num = lon * 0.017453292519943295;
        double x = 6378137.0 * num;
        double a = lat * 0.017453292519943295;

        lon = x;
        lat = 3189068.5 * Math.Log((1.0 + Math.Sin(a)) / (1.0 - Math.Sin(a)));

        double webMercatorX = x;
        double webMercatorY = 3189068.5 * Math.Log((1.0 + Math.Sin(a)) / (1.0 - Math.Sin(a)));

        return new Coord((float)webMercatorX, (float)webMercatorY);
    }

    public static Coord WorldCoordinateToWebMercator(Coord worldCoord, VectorGeometries geometries)
    {
        // Get how much each coordinate is transformed when the geometries are moved into origo. 
        float adjustmentX = geometries.GetBbox()["min"].X - geometries.GetTransformedBbox()["min"].X;
        float adjustmentY = geometries.GetBbox()["min"].Y - geometries.GetTransformedBbox()["min"].Y;

        return new Coord(worldCoord.X + adjustmentX, worldCoord.Y + adjustmentY);
    }

    public static Coord WebMercatorToWorldCoordinate(Coord webMercatorCoord, VectorGeometries geometries)
    {
        // Get how much each coordinate is transformed when the geometries are moved into origo. 
        float adjustmentX = geometries.GetBbox()["min"].X - geometries.GetTransformedBbox()["min"].X;
        float adjustmentY = geometries.GetBbox()["min"].Y - geometries.GetTransformedBbox()["min"].Y;

        return new Coord(webMercatorCoord.X - adjustmentX, webMercatorCoord.Y - adjustmentY);
    }
}