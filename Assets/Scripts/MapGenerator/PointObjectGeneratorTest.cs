﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointObjectGeneratorTest : MonoBehaviour {

    public GameObject pmerkkiGo;

    List<GameObject> busStopGos = new List<GameObject>();


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GenerateBusStops(VectorGeometries geometries)
    {
        foreach (GameObject go in busStopGos)
        {
            Destroy(go);
        }

        // Get terrain base height
        GameObject mapGenerator = GameObject.Find("MapGenerator");
        TerrainGenerator terrainGenerator = mapGenerator.GetComponent<TerrainGenerator>();
        int terrainBaseHeight = terrainGenerator.terrainBaseHeight;

        List<Geometry> busStops = geometries.GetGeometriesWithTagAndProperty("highway", "bus_stop", "Point");

        // Generate bus stops
        foreach(Geometry point in busStops)
        {
            GameObject go = Instantiate(pmerkkiGo);
            busStopGos.Add(go); // to enable destroy with map update

            Coord pointCoordinates = point.getTransformedCoordinates()[0][0];

            go.transform.position = new Vector3(pointCoordinates.X, terrainBaseHeight, pointCoordinates.Y);
            go.transform.localScale = new Vector3(0.55f, 1.5f, 0.55f);

        }
    }

}
