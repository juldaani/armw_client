﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingGenerator : MonoBehaviour {

    List<GameObject> buildings = new List<GameObject>();
    public GameObject wallPrefab;
    Vector3 wallSize;
    float minX = 0, minY = 0, maxX = 0, maxY = 0;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GenerateBuildings(VectorGeometries geometries, Terrain terrain)
    {

        foreach (GameObject go in buildings)
        {
            Destroy(go);
        }

            
        GeometryCollection geoCol = geometries.GetGeometriesWithTag("building");
        List<Polygon> buildingPolys = geoCol.GetPolygons();
        foreach(Polygon poly in buildingPolys)
        {
            List<Coord> cl = poly.getTransformedCoordinates()[0];

            // Juho lisännyt ****
            float[] minMaxHeight = GetBuildingMinMaxHeight(cl, terrain);
            float maxHeight = minMaxHeight[1];
            float minHeight = minMaxHeight[0];
            // ******       

            float height = Random.value * 30;
            if (height < 10)
                height = 10;

            height = maxHeight - minHeight + 20; // remove this !!!!!!!!!!!!!!!!

            // calculate ceter point
            float minX = float.MaxValue, minY=float.MaxValue, maxX=0, maxY=0;
            for (int i = 0; i < cl.Count - 1; i++)
            {
                Coord curr = cl[i];
                if (minX > curr.X) minX = curr.X;
                if (minY > curr.Y) minY = curr.Y;
                if (maxX < curr.X) maxX = curr.X;
                if (maxY < curr.Y) maxY = curr.Y;
            }
            float centerX = minX + ((maxX - minX) / 2);
            float centerY = minY + ((maxY - minY) / 2);



            int wallCountDebug = 0;
            // make wall
            for (int i = 0; i < cl.Count-1; i++)
            {
                Coord curr = cl[i];
                Coord next = cl[i+1];            
                                
                GameObject go = Instantiate(wallPrefab);
                buildings.Add(go); // to enable destroy with map update

                // Juho lisännyt ****
                //GameObject mapGenerator = GameObject.Find("MapGenerator");
                //TerrainGenerator terrainGenerator = mapGenerator.GetComponent<TerrainGenerator>();
                //int terrainBaseHeight = terrainGenerator.terrainBaseHeight;
                // *****

                float angle = calculateAngle(curr, next);
                double length = calculateLength(curr, next);

                wallSize = go.GetComponent<Renderer>().bounds.size; 
                float lengthScale = (float)length / wallSize.z;
                go.transform.localScale = new Vector3(1, height, lengthScale);
                Vector3 newWallSize = go.GetComponent<Renderer>().bounds.size;

                go.transform.position = new Vector3(curr.X, newWallSize.y / 2 + minHeight-1.0f, curr.Y);
                go.transform.rotation = Quaternion.Euler(0, 0, 0);
                go.transform.Rotate(0, angle, 0);
                go.transform.Translate(new Vector3(-0.5F, 0, -newWallSize.z / 2));

                //////////////// roof          
                Mesh mesh = go.GetComponent<MeshFilter>().mesh;
                Vector3[] verts = mesh.vertices;
                for (int j = 0; j < verts.Length; j++)
                {
                    // 5 & 9 are the inner top vertices of a wall
                    if (j == 5)
                    {
                        verts[j] = verts[j] + new Vector3(verts[j].x - 5, (verts[j].y+4)/height, (verts[j].z+5)/lengthScale);
                    }
                    if (j == 9)
                    {
                        verts[j] = verts[j] + new Vector3(verts[j].x - 5, (verts[j].y+4)/height, (verts[j].z-5)/lengthScale);
                    }
                }
                mesh.vertices = verts;
                mesh.RecalculateNormals();     
            }
        }
    }


    private float[] GetBuildingMinMaxHeight(List<Coord> buildingCoords, Terrain terrain)     
    {
        float minHeight = float.MaxValue;
        float maxHeight = float.MinValue;

        foreach (Coord coord in buildingCoords)
        {
            Vector3 tmp = new Vector3(coord.X, 0.0f, coord.Y);
            float height = terrain.SampleHeight(tmp);

            if(height < minHeight){
                minHeight = height;
            }
            else if( height > maxHeight)
            {
                maxHeight = height;
            }
        }

        return new float[] { minHeight, maxHeight };
    }


    private float calculateAngle(Coord c1, Coord c2)
    {
        float xDiff = c2.X - c1.X;
        float yDiff = c2.Y - c1.Y;
        return (float)(System.Math.Atan2(-yDiff, xDiff) * 180.0 / System.Math.PI)-90;
    }
    private double calculateLength(Coord c1, Coord c2)
    {
        return System.Math.Sqrt(
                    System.Math.Pow((c1.X - c2.X), 2)
                    + System.Math.Pow((c1.Y - c2.Y), 2));
    }
}
