﻿/*
 * Single rasterized geometry is stored here. The geometry is rasterized according to terrain's alphamap.
 */

using System.Collections.Generic;

public class RasterGeometry {

    // If rasterized geometry is polygon, then index 0 is rasterized polygon area and index 1 is polygon boundary.
    // If linestring, then only index 0 is used.
    private AlphamapIndices[] indices; 
    private Dictionary<string, string> properties;
    private string geometryType;    // Polygon/linestring

    // Constructor for linestrings
    public RasterGeometry(AlphamapIndices lineStringIdx, Dictionary<string, string> properties)
    {
        indices = new AlphamapIndices[1];
        indices[0] = lineStringIdx;
        this.properties = properties;
        geometryType = "Linestring";
    }


    // Constructor for polygons
    public RasterGeometry(AlphamapIndices polygonAreaIdx, AlphamapIndices polygonBoundaryIdx, Dictionary<string, string> properties)
    {
        indices = new AlphamapIndices[2];
        indices[0] = polygonAreaIdx;
        indices[1] = polygonBoundaryIdx;
        this.properties = properties;
        geometryType = "Polygon";
    }


    // Getters & setters
    public AlphamapIndices[] GetRaster()
    {
        return indices;
    }

    public Dictionary<string, string> GetProperties()
    {
        return properties;
    }

    public string GetGeometryType()
    {
        return geometryType;
    }
}
