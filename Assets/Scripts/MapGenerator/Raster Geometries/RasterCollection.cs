﻿/*
 * Rasterized geometries for a single tag (natural, waterway,...) are stored here.
 */

using System.Collections.Generic;


public class RasterCollection {

    private string tag;
    private List<RasterGeometry> polygons;
    private List<RasterGeometry> lineStrings;

    // Constructor
    public RasterCollection(string tag)
    {
        this.tag = tag;
        polygons = new List<RasterGeometry>();
        lineStrings = new List<RasterGeometry>();
    }


    // Getters & setters
    public List<RasterGeometry> GetLineStrings()
    {
        return lineStrings;
    }

    public List<RasterGeometry> GetPolygons()
    {
        return polygons;
    }
    
    public void AddPolygon(RasterGeometry polyg)
    {
        if (polyg.GetGeometryType().Equals("Polygon"))
        {
            polygons.Add(polyg);
        }
        else
        {
            throw new System.ArgumentException(polyg.GetGeometryType() + " is not allowed type. Type should be 'Polygon'.");
        }
    }

    public void AddLineString(RasterGeometry lineStr)
    {
        if (lineStr.GetGeometryType().Equals("Linestring"))
        {
            lineStrings.Add(lineStr);
        }
        else
        {
            throw new System.ArgumentException(lineStr.GetGeometryType() + " is not allowed type. Type should be 'Linestring'.");
        }
    }

}
