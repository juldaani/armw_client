﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RasterGeometries : MonoBehaviour {

    private Dictionary<string, RasterCollection> rasterCollections;     // Rasterized geometries for a certain tag (key in dict)

    // All rasterized geometries stored in the same 2D-array (same size as alphamap). Each cell contains properties of a geometry crossing the cell.
    private Dictionary<string, List<string>>[,] gridRasterizedGeoms;


    public void RasterizeGeometries(VectorGeometries vectorGeometries, float worldLengthX, float worldLengthZ, int resolution)
    {
        Rasterizer rasterizer = new Rasterizer(worldLengthX, worldLengthZ, resolution, vectorGeometries.GetTransformedBbox() );

        Dictionary<string, GeometryCollection> vectorCollection = vectorGeometries.GetGeometryCollections();
        rasterCollections = new Dictionary<string, RasterCollection>();

        gridRasterizedGeoms = new Dictionary<string, List<string>>[resolution, resolution];

        // Iterate over the dictionary
        foreach (KeyValuePair<string, GeometryCollection> entry in vectorCollection)    // Loop tags
        {
            GeometryCollection geometriesWithTag = entry.Value;
            RasterCollection tmpRasterCollection = new RasterCollection(entry.Key);

            // Rasterize polygons and polygon boundaries
            List<Polygon> polygons = geometriesWithTag.GetPolygons();
            List<AlphamapIndices> rasterizedPolygAreas = rasterizer.Rasterize(polygons, true);   // Polygon areas
            List<AlphamapIndices> rasterizedPolygBoundaries = rasterizer.Rasterize(polygons, false);   // Polygon boundaries

            // Put rasterized polygons into raster collection
            RasterGeometry tmpRasterGeom = null;
            for (int i = 0; i < rasterizedPolygAreas.Count; i++)     // Loop rasterized polygons
            {
                tmpRasterGeom = new RasterGeometry(rasterizedPolygAreas[i], rasterizedPolygBoundaries[i],
                    polygons[i].GetProperties());
                tmpRasterCollection.AddPolygon(tmpRasterGeom);
            }

            // Rasterize linestrings
            List<LineString> linestrings = geometriesWithTag.GetLineStrings();
            List<AlphamapIndices> rasterizedLinestrings = rasterizer.Rasterize(linestrings, false);

            // Put rasterized linestrings into raster collection
            for (int i = 0; i < rasterizedLinestrings.Count; i++)     // Loop rasterized linestrings
            {
                tmpRasterGeom = new RasterGeometry(rasterizedLinestrings[i], linestrings[i].GetProperties());
                tmpRasterCollection.AddLineString(tmpRasterGeom);
            }

            rasterCollections.Add(entry.Key, tmpRasterCollection);
        }


        // Create grid
        foreach (KeyValuePair<string, RasterCollection> entry in rasterCollections)    // Loop tags in the collection
        {
            RasterCollection rasterCollection = entry.Value;

            // Get linestrings and polygons
            List<RasterGeometry> rasterGeoms = new List<RasterGeometry>();
            rasterGeoms.AddRange(rasterCollection.GetLineStrings() );   
            rasterGeoms.AddRange(rasterCollection.GetPolygons() );
            foreach(RasterGeometry geom in rasterGeoms)     // Loop all rasterized geometries
            {

                AlphamapIndices[] indicesArray = geom.GetRaster();
                foreach(AlphamapIndices alphamapIndices in indicesArray)     // Loop the array of indices (1 item for linestring, 2 items for polygon)
                {

                    foreach(Idx idx in alphamapIndices.GetIndices())    // Loop indices 
                    {

                        Dictionary<string, string> properties = geom.GetProperties();
                        foreach (KeyValuePair<string, string> prop in properties)    // Iterate over the dictionary of properties
                        {

                            if (gridRasterizedGeoms[idx.X, idx.Y] == null )     // If not initialized
                            {
                                gridRasterizedGeoms[idx.X, idx.Y] = new Dictionary<string, List<string>>();
                            }

                            List<string> curList = null;
                            if (gridRasterizedGeoms[idx.X, idx.Y].ContainsKey(prop.Key) )
                            {
                                curList = gridRasterizedGeoms[idx.X, idx.Y][prop.Key];
                            }
                            else
                            {
                                curList = new List<string>();
                            }

                            curList.Add(prop.Value);
                            gridRasterizedGeoms[idx.X, idx.Y][prop.Key] = curList;  // Add value to grid

                        }
                    }
                }
            }
        }

    }


    // Getters & setters
    public RasterCollection GetGeometriesWithTag(string tag)
    {
        RasterCollection collection = null;

        try
        {
            collection = rasterCollections[tag];
        }
        catch (KeyNotFoundException e)
        {
            Debug.Log(e);
        }

        return collection;
    }

    public List<RasterGeometry> GetGeometriesWithTag(string tag, string type)
    {
        RasterCollection collection = null;
        List<RasterGeometry> tmp = new List<RasterGeometry>();

        try
        {
            collection = GetGeometriesWithTag(tag);

            if (collection != null && type.Equals("Polygon"))
            {
                tmp = collection.GetPolygons();
            }
            else if (collection != null && type.Equals("Linestring"))
            {
                tmp = collection.GetLineStrings();
            }
            else if(collection != null)
            {
                Debug.LogError("Error!  " + type + " is not supported geometry type!");
            }
        }
        catch (KeyNotFoundException e)
        {
            Debug.Log(e);
        }

        return tmp;
    }

    public List<RasterGeometry> GetGeometriesWithTagAndProperty(string tag, string property, string type)
    {
        RasterCollection collection = null;
        List<RasterGeometry> tmp = new List<RasterGeometry>();

        try
        {
            collection = GetGeometriesWithTag(tag);
        }
        catch (KeyNotFoundException e)
        {
            Debug.Log(e);
        }

        // Filter geometries according to the type and property
        if (collection != null && type.Equals("Polygon"))
        {
            List<RasterGeometry> geoms = collection.GetPolygons();

            foreach (RasterGeometry polyg in geoms)
            {
                if (polyg.GetProperties()[tag].Equals(property))    // Pick only geometries having the desired property
                {
                    tmp.Add(polyg);
                }
            }
        }

        else if (collection != null && type.Equals("Linestring"))
        {
            List<RasterGeometry> geoms = collection.GetLineStrings();

            foreach (RasterGeometry linestr in geoms)
            {
                if (linestr.GetProperties()[tag].Equals(property))    // Pick only geometries having the desired property
                {
                    tmp.Add(linestr);
                }
            }
        }

        else if (collection != null)
        {
            Debug.LogError("Error!  " + type + " is not supported geometry type!");
        }

        return tmp;
    }

    public static List<AlphamapIndices> GetAlphamapIndices(List<RasterGeometry> rasterizedGeometries, bool getPolygonArea)
    {
        List<AlphamapIndices> indices = new List<AlphamapIndices>();

        foreach (RasterGeometry geom in rasterizedGeometries)
        {
            if (geom.GetType().Equals("Polygon") && getPolygonArea == false)
            {
                indices.Add(geom.GetRaster()[1]);
            }
            else
            {
                indices.Add(geom.GetRaster()[0]);
            }
        }

        return indices;
    }

    public Dictionary<string, List<string>>[,] GetGrid()
    {
        return gridRasterizedGeoms;
    }
}
