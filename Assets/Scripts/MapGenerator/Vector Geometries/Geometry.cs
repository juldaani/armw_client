﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*************
 * Classes for different geometries (polygon, linestring, point)
**************/
public abstract class Geometry
{

    protected List<List<Coord>> coordinates;    // Double lists because polygons can have interior rings
    protected List<List<Coord>> transformedCoordinates;
    protected Coord transformedBboxMinXY;     // Geometry's bbox center point transformed to the origin
    protected Coord transformedBboxMaxXY;
    protected Dictionary<string, string> properties;

    // Constructor
    public Geometry(List<List<Coord>> coordinates, Dictionary<string, string> properties, Coord mapBlockGenerationCoordinateXY)
    {
        this.coordinates = coordinates;
        transformedCoordinates = transformToOrigin(coordinates, mapBlockGenerationCoordinateXY);

        this.properties = properties;
    }


    // Transform geometry to the origin (center point of the global bbox is in the origin)
    private List<List<Coord>> transformToOrigin(List<List<Coord>> coordinates, Coord mapBlockGenerationCoordinate) 
    {
        List<List<Coord>> tmpCoordinates = new List<List<Coord>>();
        Coord bboxMin = null, bboxMax = null;

        foreach (List<Coord> curList in coordinates)
        {
            List<Coord> tmpList = new List<Coord>();

            foreach (Coord coordinate in curList)
            {
                // Transform geometry
                float newX = coordinate.X - mapBlockGenerationCoordinate.X;
                float newY = coordinate.Y - mapBlockGenerationCoordinate.Y;

                tmpList.Add(new Coord(newX, newY));

                // Pick bbox of a geometry
                if (bboxMin == null)    // If first round
                {
                    bboxMin = new Coord(newX, newY);
                    bboxMax = new Coord(newX, newY);
                }
                else
                {
                    // Find min and max
                    if (newX < bboxMin.X)
                        bboxMin.X = newX;
                    if (newY < bboxMin.Y)
                        bboxMin.Y = newY;
                    if (newX > bboxMax.X)
                        bboxMax.X = newX;
                    if (newY > bboxMax.Y)
                        bboxMax.Y = newY;
                }
            }

            tmpCoordinates.Add(tmpList);
        }

        transformedBboxMaxXY = bboxMax;
        transformedBboxMinXY = bboxMin;

        return tmpCoordinates;
    }


    // Getters & setters
    public List<List<Coord>> getTransformedCoordinates()
    {
        return transformedCoordinates;
    }

    public Dictionary<string, Coord> GetTransformedBbox()
    {
        Dictionary<string, Coord> tmp = new Dictionary<string, Coord>();
        tmp.Add("min", transformedBboxMinXY);
        tmp.Add("max", transformedBboxMaxXY);
        return tmp;
    }

    public List<List<Coord>> getCoordinates()
    {
        return coordinates;
    }

    public Dictionary<string, string> GetProperties()
    {
        return properties;
    }
}
// END OF GEOMETRY CLASS