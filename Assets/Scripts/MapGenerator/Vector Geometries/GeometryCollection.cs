﻿using System.Collections.Generic;

/***
 * All geometries for a specific tag are stored in GeometryCollection
***/
public class GeometryCollection
{

    private string tag;
    private List<Polygon> polygons;
    private List<LineString> lineStrings;
    private List<Point> points;
    private Coord bboxMinXY;
    private Coord bboxMaxXY;

    // Constructor
    public GeometryCollection(string tag, Coord bboxMinXY, Coord bboxMaxXY)
    {
        this.tag = tag;
        this.bboxMaxXY = bboxMaxXY;
        this.bboxMinXY = bboxMinXY;
        polygons = new List<Polygon>();
        lineStrings = new List<LineString>();
        points = new List<Point>();
    }

    // Getters & setters
    public List<LineString> GetLineStrings()
    {
        return lineStrings;
    }

    public List<Polygon> GetPolygons()
    {
        return polygons;
    }

    public List<Point> GetPoints()
    {
        return points;
    }

    public Coord GetBboxMinXY()
    {
        return bboxMinXY;
    }

    public Coord GetBboxMaxXY()
    {
        return bboxMaxXY;
    }

    public void AddPolygon(Polygon polyg)
    {
        polygons.Add(polyg);
    }

    public void AddPoint(Point point)
    {
        points.Add(point);
    }

    public void AddLineString(LineString lineStr)
    {
        lineStrings.Add(lineStr);
    }
}
// END OF GEOMETRY COLLECTION CLASS
