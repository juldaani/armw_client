﻿using System.Collections.Generic;

public class Point : Geometry
{

    // Constructor
    public Point(List<List<Coord>> coordinates, Dictionary<string, string> properties, Coord bboxMinXy,
        Coord bboxMaxXY, Coord mapBlockGenerationCoordinate) : base(coordinates, properties, mapBlockGenerationCoordinate)
    {
        foreach (List<Coord> coords in coordinates)
        {
            // Prevent a point having multiple coordinate values
            if (coords.Count > 1)
            {
                throw new System.ArgumentException("Cannot add more than one coordinate value for a " +
                    "point geometry type.", "coordinates");
            }
        }
    }
}
