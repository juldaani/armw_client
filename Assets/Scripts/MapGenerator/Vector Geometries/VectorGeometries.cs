﻿/*
 * The class parses GeoJson data with SimpleJSON and converts it into geometry objects 
 * (polygon, point, line string).
*/

using UnityEngine;
using SimpleJSON;
using System.Collections.Generic;


public class VectorGeometries {

    private string projectionSRID { get; set; }
    private List<string> tags { get; set; }
    private Coord bboxMinXY { get; set; }   // Bbox in the web mercator
    private Coord bboxMaxXY { get; set; }
    private Coord transformedBboxMinXY;     // Bbox in the world coordinates
    private Coord transformedBboxMaxXY;
    private Dictionary<string, GeometryCollection> geometryCollections;


    // Constructor
    public VectorGeometries(string jsonData, int offset, float longitude, float latitude)
    {
        Coord mapBlockGenerationCoordinateXY = CoordinateConverter.ToWebMercator(longitude, latitude);

        // Default values (in the case if the map block is empty)
        bboxMinXY = new Coord(mapBlockGenerationCoordinateXY.X - offset, mapBlockGenerationCoordinateXY.Y - offset);
        bboxMaxXY = new Coord(mapBlockGenerationCoordinateXY.X + offset, mapBlockGenerationCoordinateXY.Y + offset); ;
        transformedBboxMinXY = new Coord(-offset, -offset);
        transformedBboxMaxXY = new Coord(offset, offset);

        tags = new List<string>();

        geometryCollections = ParseJson(jsonData, mapBlockGenerationCoordinateXY);

        // Move bbox center point to the origin
        if (bboxMinXY != null && bboxMaxXY != null)
        {
            /*
            float centerBbox_X = (bboxMaxXY.X - bboxMinXY.X) / 2 + bboxMinXY.X;
            float centerBbox_Y = (bboxMaxXY.Y - bboxMinXY.Y) / 2 + bboxMinXY.Y;
            transformedBboxMinXY = new Coord(bboxMinXY.X - centerBbox_X, bboxMinXY.Y - centerBbox_Y);
            transformedBboxMaxXY = new Coord(bboxMaxXY.X - centerBbox_X, bboxMaxXY.Y - centerBbox_Y);
            */

            
            transformedBboxMinXY = new Coord(bboxMinXY.X - mapBlockGenerationCoordinateXY.X, 
                bboxMinXY.Y - mapBlockGenerationCoordinateXY.Y);
            transformedBboxMaxXY = new Coord(bboxMaxXY.X - mapBlockGenerationCoordinateXY.X, 
                bboxMaxXY.Y - mapBlockGenerationCoordinateXY.Y);
            
        }
    }


    /*
     * Getters & setters
     */
    public Dictionary<string, GeometryCollection> GetGeometryCollections()
    {
        return geometryCollections;
    }

    public GeometryCollection GetGeometriesWithTag(string tag)
    {
        GeometryCollection collection = null;

        try
        {
            collection = geometryCollections[tag];
        }
        catch (KeyNotFoundException e)
        {
            Debug.Log(e);
        }

        return collection;
    }

    public List<Geometry> GetGeometriesWithTagAndProperty(string tag, string property, string type)
    {
        GeometryCollection collection = null;
        List<Geometry> tmp = new List<Geometry>();

        try
        {
            collection = GetGeometriesWithTag(tag);
        }
        catch(KeyNotFoundException e)
        {
            Debug.Log(e);
        }

        // Filter geometries according to the type and property
        if (collection != null && type.Equals("Polygon"))
        {
            List<Polygon> geoms = collection.GetPolygons();

            foreach (Polygon polyg in geoms)
            {
                if (polyg.GetProperties()[tag].Equals(property))    // Pick only geometries having the desired property
                {
                    tmp.Add(polyg);
                }
            }
        }

        else if(collection != null && type.Equals("Linestring")){
            List<LineString> geoms = collection.GetLineStrings();

            foreach (LineString linestr in geoms)
            {
                if (linestr.GetProperties()[tag].Equals(property))    // Pick only geometries having the desired property
                {
                    tmp.Add(linestr);
                }
            }
        }

        else if (collection != null && type.Equals("Point"))
        {
            List<Point> geoms = collection.GetPoints();

            foreach (Point point in geoms)
            {
                if (point.GetProperties()[tag].Equals(property))    // Pick only geometries having the desired property
                {
                    tmp.Add(point);
                }
            }
        }

        else if (collection != null)
        {
            Debug.LogError("Error!  " + type + " is not supported geometry type!");
        }

        return tmp;
    }

    public Dictionary<string, Coord> GetTransformedBbox()
    {
        Dictionary<string, Coord> tmp = new Dictionary<string, Coord>();
        tmp.Add("min", transformedBboxMinXY);
        tmp.Add("max", transformedBboxMaxXY);
        return tmp;
    }

    public Dictionary<string, Coord> GetBbox()
    {
        Dictionary<string, Coord> tmp = new Dictionary<string, Coord>();
        tmp.Add("min", bboxMinXY);
        tmp.Add("max", bboxMaxXY);
        return tmp;
    }


    /*
     *  Parse GeoJson to geometry objects
    */
    private Dictionary<string, GeometryCollection> ParseJson(string jsonData, Coord mapBlockGenerationCoordinate)
    {
        Dictionary<string, GeometryCollection> tmpGeomCollectionDict = new Dictionary<string, GeometryCollection>();
        JSONNode jsonNode = JSON.Parse (jsonData);
        projectionSRID = jsonNode["crs"]["properties"]["name"];

        // Get tags in the json response
        foreach (var key in jsonNode.Keys)   // Loop key-value pairs in properties
        {
            if(!key.ToString().Equals("crs"))     // Leave out the key for a coordinate system
            {
                tags.Add(key.ToString());
            }
        }

        // Find a global bounding box of all geometry collections
        bool firstRound = true;
        foreach (string tag in tags)
        {
            JSONNode bbox = jsonNode[tag]["bbox"];
            Coord tmpBboxMinXY = new Coord(bbox[0].AsFloat, bbox[1].AsFloat);
            Coord tmpBboxMaxXY = new Coord(bbox[2].AsFloat, bbox[3].AsFloat);

            if (firstRound)
            {
                firstRound = false;
                bboxMinXY = tmpBboxMinXY;
                bboxMaxXY = tmpBboxMaxXY;
            }

            // Update bbox values (find min & max)
            if (tmpBboxMinXY.X < bboxMinXY.X)
            {
                bboxMinXY.X = tmpBboxMinXY.X;
            }
            if (tmpBboxMinXY.Y < bboxMinXY.Y)
            {
                bboxMinXY.Y = tmpBboxMinXY.Y;
            }
            if (tmpBboxMaxXY.X > bboxMaxXY.X)
            {
                bboxMaxXY.X = tmpBboxMaxXY.X;
            }
            if (tmpBboxMaxXY.Y > bboxMaxXY.Y)
            {
                bboxMaxXY.Y = tmpBboxMaxXY.Y;
            }
        }

        // Create geometry objects (point, polygon, linestring)
        foreach (string tag in tags)    // Loop all tags
        {   
            JSONNode features = jsonNode[tag]["features"];  // All features associated with a certain tag

            // Get bbox
            JSONNode bbox = jsonNode[tag]["bbox"];
            Coord tagBboxMinXY = new Coord(bbox[0].AsFloat, bbox[1].AsFloat);
            Coord tagBboxMaxXY = new Coord(bbox[2].AsFloat, bbox[3].AsFloat);

            GeometryCollection geomCollection = new GeometryCollection(tag, tagBboxMinXY, tagBboxMaxXY);

            // Create geometry objects
            for (int i = 0; i < features.Count; i++)    // Loop features
            {
                JSONNode curFeature = features[i];
                JSONNode geom = curFeature["geometry"];
                string geomType = geom["type"].Value;

                // Different geometry types have different number of lists in GeoJson so they are
                // handled separately, e.g. polygons with interior rings have multiple lists of coordinates
                JSONNode geomCoordinates = null;
                List<List<Coord>> coordinateList = new List<List<Coord>>();
                geomCoordinates = geom["coordinates"];

                // Get coordinates of a point
                if (geomType.Equals("Point"))
                {
                    float x = geomCoordinates[0].AsFloat;
                    float y = geomCoordinates[1].AsFloat;
                    Coord tmpCoord = new Coord(x, y);
                    List<Coord> tmpList = new List<Coord>();
                    tmpList.Add(tmpCoord);
                    coordinateList.Add(tmpList);
                }

                // Get coordinates of a linestring
                else if (geomType.Equals("LineString"))
                {
                    List<Coord> tmpList = new List<Coord>();

                    for (int k = 0; k < geomCoordinates.Count; k++) // Loop geometry's coordinates
                    {
                        JSONNode curCoordinate = geomCoordinates[k];
                        float x = curCoordinate[0].AsFloat;
                        float y = curCoordinate[1].AsFloat;
                        Coord tmpCoord = new Coord(x, y);
                        tmpList.Add(tmpCoord);
                    }
                    coordinateList.Add(tmpList);
                }

                // Get coordinates of a polygon
                else if (geomType.Equals("Polygon"))
                {
                    for (int p = 0; p < geomCoordinates.Count; p++) // Loop geometry's coordinates
                    {
                        List<Coord> tmpList = new List<Coord>();
                        JSONNode singlePolygonCoords = geomCoordinates[p];

                        for (int k = 0; k < singlePolygonCoords.Count; k++) {
                            JSONNode curCoordinate = singlePolygonCoords[k];
                            float x = curCoordinate[0].AsFloat;
                            float y = curCoordinate[1].AsFloat;
                            Coord tmpCoord = new Coord(x, y);
                            tmpList.Add(tmpCoord);
                        }
                        coordinateList.Add(tmpList);
                    }
                }

                // Get feature's properties
                JSONNode properties = curFeature["properties"];
                Dictionary<string, string> propertiesDict = new Dictionary<string, string>();
                foreach (var key in properties.Keys)   // Loop key-value pairs in properties
                {
                    string value = properties[key.ToString()].Value;
                    string stringKey = key.ToString();
                    propertiesDict.Add(stringKey, value);
                }

                // Create geometry objects
                try
                {
                    if (geomType.Equals("Polygon"))
                    {
                        Polygon polygon = new Polygon(coordinateList, propertiesDict, bboxMinXY, bboxMaxXY, mapBlockGenerationCoordinate);
                        geomCollection.AddPolygon(polygon);
                    }
                    else if (geomType.Equals("Point"))
                    {
                        Point point = new Point(coordinateList, propertiesDict, bboxMinXY, bboxMaxXY, mapBlockGenerationCoordinate);
                        geomCollection.AddPoint(point);
                    }
                    else if (geomType.Equals("LineString"))
                    {
                        LineString lineString = new LineString(coordinateList, propertiesDict, bboxMinXY, 
                            bboxMaxXY, mapBlockGenerationCoordinate);
                        geomCollection.AddLineString(lineString);
                    }
                }
                catch (System.ArgumentException e)
                {
                    Debug.Log(e);
                }
            }

            tmpGeomCollectionDict.Add(tag, geomCollection);
        }

        return tmpGeomCollectionDict;
	}
    // * * * * * * * * * * * * * * *
    // END OF ParseJson()


}
// END OF VECTOR GEOMETRIES CLASS
