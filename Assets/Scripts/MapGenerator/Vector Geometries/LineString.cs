﻿using System.Collections.Generic;


public class LineString : Geometry
{

    // Constructor
    public LineString(List<List<Coord>> coordinates, Dictionary<string, string> properties, Coord bboxMinXy,
        Coord bboxMaxXY, Coord mapBlockGenerationCoordinate) : base(coordinates, properties, mapBlockGenerationCoordinate)
    {
        // Prevent a line string having less than 2 coordinate values
        foreach (List<Coord> coords in coordinates)
        {
            if (coords.Count < 2)
            {
                throw new System.ArgumentException("At least 2 coordinates values are required for " +
                    "a line string geometry type.", "coordinates");
            }
        }
    }
}