﻿using System.Collections.Generic;

public class Polygon : Geometry
{

    // Constructor
    public Polygon(List<List<Coord>> coordinates, Dictionary<string, string> properties, Coord bboxMinXy,
        Coord bboxMaxXY, Coord mapBlockGenerationCoordinate) : base(coordinates, properties, mapBlockGenerationCoordinate)
    {
        // Give warning if a polygon has less than 4 coordinate values
        foreach (List<Coord> coords in coordinates)
        {
            if (coords.Count < 4)
            {
                throw new System.ArgumentException("At least 4 coordinate values are required for " +
                    "a polygon geometry type.", "coordinates");
            }
        }
    }

}
