﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Heightmap parameters (amplitudes, frequencies, height ratio) for each pixel in the heightmap are stored in this class.
 */
public class HeightmapParameters{

    private float[] frequencies;
    private float[] amplitudes;
    private float heightRatio;

    private readonly float initialHeightRatio;
    private readonly float[] initialAmplitudes;
    private readonly float sumOfInitialAmplitudes;

    public HeightmapParameters(float[] frequencies, float[] amplitudes, float heightRatio)
    {
        if(frequencies.GetLength(0) != amplitudes.GetLength(0) )
        {
            throw new System.ArgumentException("Frequencies and amplitudes should have the same number of items!");
        }

        this.amplitudes = amplitudes;
        initialAmplitudes = (float[])amplitudes.Clone();
        initialHeightRatio = heightRatio;

        sumOfInitialAmplitudes = 0.0f;
        for (int i = 0; i < initialAmplitudes.GetLength(0); i++)    // Calculate sum of amplitudes
        {
            sumOfInitialAmplitudes += amplitudes[i];
        }

        this.frequencies = frequencies;
        this.heightRatio = heightRatio;
    }

    // Getters & setters
    public void SetFrequencies(float[] frequencies)
    {
        this.frequencies = frequencies;
    }

    public void SetAmplitudes(float[] amplitudes)
    {
        float sumOfAmplitudes = 0.0f;
        for (int i = 0; i < amplitudes.GetLength(0); i++)    // Calculate sum of amplitudes
        {
            sumOfAmplitudes += amplitudes[i];
        }

        // Prevent assigning higher amplitude values than default. This is done because otherwise scaling would be incorrect.
        if (sumOfAmplitudes <= sumOfInitialAmplitudes)   
        {
            this.amplitudes = amplitudes;
        }
        else
        {
            throw new System.ArgumentException("Sum of amplitudes to be assigned is higher than the sum of initial amplitudes.");
        }
    }

    public void SetHeightRatio(float heightRatio)
    {
        this.heightRatio = Mathf.Clamp(heightRatio, 0.0f, 1.0f);
    }

    public float[] GetFrequencies()
    {
        return frequencies;
    }

    public float[] GetAmplitudes()
    {
        return amplitudes;
    }

    public float[] GetInitialAmplitudes()
    {
        return initialAmplitudes;
    }

    public float GetInitialHeightRatio()
    {
        return initialHeightRatio;
    }

    public float GetHeightRatio()
    {
        return heightRatio;
    }
} 
// *  * * * * * * ** END OF class  HeightParameters * * * * * * * * *




public static class HeightmapGenerator {


    /*
     * Make mask which controls how GenerateHeightMap() creates a heightmap.
     * - Neighborhoods around rivers are smoothed, i.e. no large height differences are allowed in immediate vicinity of rivers.
     */
    private static HeightmapParameters[,] MakeMask(int mapWidth, int mapHeight, RasterGeometries rasterGeometries, int radius, 
        float[] defaultFrequencies, float[] defaultAmplitudes, float defaultHeightRatio)
    {
        // Initialize heightmap parameters for the mask
        HeightmapParameters[,] mask = new HeightmapParameters[mapWidth, mapHeight];

        // Populate mask with default parameters
        for (int x = 0; x < mapWidth; x++)   
        {
            for (int y = 0; y < mapHeight; y++)
            {
                mask[x, y] = new HeightmapParameters(defaultFrequencies, defaultAmplitudes, defaultHeightRatio);
            }
        }

        // Get rasterized geometries for water
        List<RasterGeometry> waterGeomList = new List<RasterGeometry>();
        waterGeomList.AddRange(rasterGeometries.GetGeometriesWithTagAndProperty("waterway", "river", "Linestring") );
        waterGeomList.AddRange(rasterGeometries.GetGeometriesWithTagAndProperty("waterway", "riverbank", "Polygon") );
        waterGeomList.AddRange(rasterGeometries.GetGeometriesWithTagAndProperty("natural", "coastline", "Linestring") );
        waterGeomList.AddRange(rasterGeometries.GetGeometriesWithTagAndProperty("natural", "coastline", "Polygon") );  // Islands
        waterGeomList.AddRange(rasterGeometries.GetGeometriesWithTagAndProperty("natural", "water", "Polygon") );
        waterGeomList.AddRange(rasterGeometries.GetGeometriesWithTagAndProperty("waterway", "stream", "Linestring") );
        waterGeomList.AddRange(rasterGeometries.GetGeometriesWithTagAndProperty("waterway", "ditch", "Linestring") );

        ProcessWaterGeometries(ref mask, ref waterGeomList, radius);

        // Get rasterized geometries for landuse
        List<RasterGeometry> areas = new List<RasterGeometry>();
        areas.AddRange(rasterGeometries.GetGeometriesWithTagAndProperty("landuse", "residential", "Polygon") );
        areas.AddRange(rasterGeometries.GetGeometriesWithTagAndProperty("landuse", "industrial", "Polygon") );
        areas.AddRange(rasterGeometries.GetGeometriesWithTagAndProperty("landuse", "commercial", "Polygon"));
        areas.AddRange(rasterGeometries.GetGeometriesWithTagAndProperty("landuse", "retail", "Polygon"));
        areas.AddRange(rasterGeometries.GetGeometriesWithTagAndProperty("landuse", "farmland", "Polygon"));

        SmoothPolygonAreas(ref mask, ref areas, 10);

        return mask;
    }


    /*
     * Smooth polygon areas
     * - Smooth amplitude changes around the polygon boundaries in order to remove sharp edges
     * - Set all but the first amplitude inside the polygon to zero (smoothening the polygon area)
     */
    private static void SmoothPolygonAreas(ref HeightmapParameters[,] mask, ref List<RasterGeometry> rasterGeomList, int radius)
    {
        int mapWidth = mask.GetLength(0);
        int mapHeight = mask.GetLength(1);

        // Precalculate smoothing window 
        float[,] smoothingWindow = new float[radius * 2 + 1, radius * 2 + 1];
        for (int x = 0; x < smoothingWindow.GetLength(0); x++)     // Loop through the window
        {
            for (int y = 0; y < smoothingWindow.GetLength(1); y++)
            {
                smoothingWindow[x, y] = 1.0f;

                int idxWindowCenter = radius;
                int diffX = x - idxWindowCenter;    // Difference along x-axis to window center 
                int diffY = y - idxWindowCenter;    // Difference along y-axis to window center 
                float squareDistToWindowCenter = diffX * diffX + diffY * diffY;

                if (squareDistToWindowCenter <= radius * radius)     // Make window circular.
                {
                    float distToWindowCenter = Mathf.Sqrt(squareDistToWindowCenter);   // True distance to window center

                    //float k = 10.0f / radius;   // Steepness of function
                    //float smoothingFunctionValue = 1 / (1 + Mathf.Exp(-k * (distToWindowCenter - (radius / 2))));     // Logistic function kernel
                    float smoothingFunctionValue = Mathf.Clamp((0.6f / (float)radius) * distToWindowCenter + (6 / (float)radius), 0.0f, 1.0f);   // Linear kernel

                    smoothingWindow[x, y] = smoothingFunctionValue;
                }
            }
        }


        foreach (RasterGeometry rasterGeom in rasterGeomList)    // Loop rasterized polygons
        {
            List<Idx> indices = new List<Idx>();

            // POLYGON BOUNDARY
            // Smooth amplitude changes around the polygon boundaries in order to remove sharp edges.
            indices = rasterGeom.GetRaster()[1].GetIndices();   // Get only polygon boundary
            foreach (Idx idx in indices)    // Loop indices
            {
                
                for (int x = -radius; x <= radius; x++)     // Loop through the smoothing window
                {
                    for (int y = -radius; y <= radius; y++)
                    {

                    
                        // Indexes corresponding to the heightmap
                        int curX = idx.X + x;
                        int curY = idx.Y + y;

                        if ((curX >= 0) && (curX < mapWidth) && (curY >= 0) && (curY < mapHeight) )     // Prevent index going out of range
                        {
                            // Convert indexes to match indexes in the precalculated smoothing window
                            int idxSmoothingWindow_X = x + radius;
                            int idxSmoothingWindow_Y = y + radius;

                            float smoothingFunctionValue = smoothingWindow[idxSmoothingWindow_X, idxSmoothingWindow_Y];

                            HeightmapParameters curParams = mask[curX, curY];   // Get parameters in the current index of the heightmap

                            // Calculate smoothed amplitudes for each octave in the current index of the window
                            float[] amplitudes = (float[])curParams.GetAmplitudes().Clone();
                            for (int i = 2; i < amplitudes.Length - 3; i++)
                            {
                                float newAmplitudeValueCandidate = smoothingFunctionValue * amplitudes[i];

                                if (newAmplitudeValueCandidate < amplitudes[i])     //Don't update if the new value is higher than the current value
                                {
                                    amplitudes[i] = newAmplitudeValueCandidate;
                                }
                                
                            }

                            curParams.SetAmplitudes(amplitudes);    // Set new amplitudes
                        }
                    }
                }
            }


            // POLYGON INNER PARTS
            // Set the amplitudes inside the polygon to zero
            indices = rasterGeom.GetRaster()[0].GetIndices();   // Get only polygon area
            foreach (Idx idx in indices)    // Loop indices
            {
                HeightmapParameters curParams = mask[idx.X, idx.Y];

                // Set all but the first amplitude to zero. This has an effect of smoothing the surface inside the polygon.
                float[] newAmplitudes = (float[])curParams.GetAmplitudes().Clone();
                for (int i=2; i<newAmplitudes.Length-1; i++)
                {
                    newAmplitudes[i] = 0.0f;
                }
                curParams.SetAmplitudes(newAmplitudes);
            }

        }
    }


    /*
     * Process water geometries.
     * The terrain around water areas (lakes, rivers etc.) must be gradually "lowered" towards the water since water is always on the same constant height.
     */
    private static void ProcessWaterGeometries(ref HeightmapParameters[,] mask, ref List<RasterGeometry> rasterGeomList, int radius)
    {
        int mapWidth = mask.GetLength(0);
        int mapHeight = mask.GetLength(1);

        
        // Precalculate smoothing window
        HeightmapParameters heightmapParams = mask[0, 0];   // Just pick some arbitrary object since initial height ratios are equal for every object in the current map block
        float maxHeightRatio = heightmapParams.GetInitialHeightRatio();   // Get maximum height ratio for the current map block
        float[,] smoothingWindow = new float[radius * 2 + 1, radius * 2 + 1];
        for (int x = 0; x < smoothingWindow.GetLength(0); x++)     // Loop through the window
        {
            for (int y = 0; y < smoothingWindow.GetLength(1); y++)
            {
                smoothingWindow[x, y] = 1.0f;

                int idxWindowCenter = radius;
                int diffX = x - idxWindowCenter;    // Difference along x-axis to window center
                int diffY = y - idxWindowCenter;    // Difference along y-axis to window center
                float squareDistToWindowCenter = diffX * diffX + diffY * diffY;

                if (squareDistToWindowCenter <= radius * radius)     // Make window circular.
                {
                    float distToWindowCenter = Mathf.Sqrt(squareDistToWindowCenter);   // True distance to window center

                    float k = 10.0f / radius;   // Steepness of function
                    float logisticFunctionValue = maxHeightRatio / (1 + Mathf.Exp(-k * (distToWindowCenter - (radius / 2))));     // Logistic function kernel

                    smoothingWindow[x, y] = logisticFunctionValue;
                }
            }
        }
        

        foreach (RasterGeometry rasterGeom in rasterGeomList)    // Loop rasterized geometries
        {
            // Get linestrings and boundaries of polygons
            List<Idx> indices = new List<Idx>();
            if (rasterGeom.GetGeometryType().Equals("Linestring"))
            {
                indices = rasterGeom.GetRaster()[0].GetIndices();
            }
            else if (rasterGeom.GetGeometryType().Equals("Polygon"))
            {
                indices = rasterGeom.GetRaster()[1].GetIndices();   // Get only polygon boundary (linestring)
            }

            foreach (Idx idx in indices)    // Loop geometries
            {
                // Apply logistic function smoothing window for water related linestrings and polygon boundaries.
                // This is done because water is always on the same height so the terrain must be gradually "lowered" towards water.
                for (int x = -radius; x <= radius; x++)     // Loop through the window
                {
                    for (int y = -radius; y <= radius; y++)
                    {
                        int curX = idx.X + x;
                        int curY = idx.Y + y;

                        if ((curX >= 0) && (curX < mapWidth) && (curY >= 0) && (curY < mapHeight) )     // Prevent index going out of range
                        {
                            HeightmapParameters curParams = mask[curX, curY];

                            int idxSmoothingWindow_X = x + radius;
                            int idxSmoothingWindow_Y = y + radius;

                            float logisticFunctionValue = smoothingWindow[idxSmoothingWindow_X, idxSmoothingWindow_Y];

                            if (logisticFunctionValue <= curParams.GetHeightRatio())
                            {
                                curParams.SetHeightRatio(logisticFunctionValue);     // Update mask
                            }
                        }
                        
                    }
                }
            }
        }
    }


    /*
     * Generate heightmap with perlin noise.
     * Tutorial: https://www.youtube.com/watch?v=MRNFcywkUSA&index=3&list=PLFt_AvWsXl0eBW2EiBtl_sxmDtSgZBxB3
     */
    public static float[,] GenerateHeightMap(int mapWidth, int mapHeight,
        float terrainBaseHeight, float terrainMaxHeight, TerrainData terrainData, VectorGeometries geometries, RasterGeometries rasterGeometries,
        int offset, Coord mapBlockGenerationCoordinateWorld)
    {
        // Default parameters for each octave
        int numFineOctaves = 5;     // Number of fine octaves
        float[] defaultAmplitudes = new float[] { 1.0f, 0.5f, 0.11f, 0.05f,
            0.07f, 0.007f, 0.0035f, 0.002f, 0.005f };  // The last n (numFineOctaves) octaves are not affected by the height ratio
        float[] defaultFrequencies = new float[] { 1.0f, 1.7f, 4.0f, 9.0f,
            4.0f, 20.0f, 40.0f, 80.0f, 200.0f };
        float defaultHeightRatio = 1.0f;
        float scale = 750.1f;

        float shrinkingFactor = 0.01f;      // Offset shrinking factor

        // Calculate max and min values for the noise (this is used for scaling height values between 0.0 and 1.0)
        float maxNoiseHeight = 0.0f;
        float minNoiseHeight = 0.0f;
        for (int i = 0; i < defaultAmplitudes.GetLength(0); i++)    // Loop amplitude array
        {
            maxNoiseHeight += defaultAmplitudes[i];
            minNoiseHeight -= defaultAmplitudes[i];
        }

        HeightmapParameters[,] mask = MakeMask(mapWidth, mapHeight, rasterGeometries, (int)(terrainMaxHeight*0.7f), defaultFrequencies, 
            defaultAmplitudes, defaultHeightRatio);

        // Get alphamap index for the map block generation coordinate and offset
        float worldUnitSize = terrainData.size.x / terrainData.heightmapResolution;
        Idx mapBlockGenerationWorldCoordinateIdx = AlphamapIndices.WorldCoordinateToAlphamapIndex(mapBlockGenerationCoordinateWorld,
            geometries.GetTransformedBbox(), terrainData.size.x, terrainData.size.z, terrainData.alphamapResolution);
        float offsetInHeightmapIdx = Mathf.Floor((offset / worldUnitSize));


        // Generate noise map (height map)
        float[,] noiseMap = new float[mapWidth, mapHeight];
        for (int y=0; y<mapHeight; y++)      // Loop noiseMap
        {
            for(int x=0; x<mapWidth; x++)
            {
                // Distance between the recent map block generation coordinate (as an alphamap index) and the current index in the heightmap.
                float squareDist = Mathf.Pow(x - mapBlockGenerationWorldCoordinateIdx.X, 2) +
                    Mathf.Pow(y - mapBlockGenerationWorldCoordinateIdx.Y, 2);

                noiseMap[y, x] = 0.0f;  // Set heights to zero if outside the offset radius

                if (squareDist < Mathf.Pow(offsetInHeightmapIdx, 2)*(1-shrinkingFactor) )   // If inside the offset radius
                                                // Shrink the offset radius also a littlebit (shrinkingFactor) in order to avoid artefacts on the map block border
                {
                    // Get world coordinate for the current noise map (heightmap) index
                    Vector3 worldCoord = AlphamapIndices.AlphamapIndexToWorldCoordinate(new Idx(x, y),
                        terrainData.size.x, terrainData.size.z, terrainData.heightmapResolution);

                    // Transform the current world coordinate to web mercator (true locations)
                    Coord webMercatorCoord = CoordinateConverter.WorldCoordinateToWebMercator(new Coord(worldCoord.x, worldCoord.z),
                        geometries);

                    HeightmapParameters curParams = mask[x, y];
                    float[] amplitudes = curParams.GetAmplitudes();
                    float[] frequencies = curParams.GetFrequencies();
                    float heightRatio = curParams.GetHeightRatio();

                    // Create first 3 octaves (the coarsest ones)
                    float noiseHeight = 0;
                    for (int i=0; i<amplitudes.GetLength(0)- numFineOctaves; i++)    // Loop n-2 items in amplitude and frequency arrays
                    {
                        float sampleX = webMercatorCoord.X / scale * frequencies[i];  // Scale the value (perlin noise gives always same values for integer coordinates)
                        float sampleY = webMercatorCoord.Y / scale * frequencies[i];

                        float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;    // Scale into range -1.0 - 1.0 (allow heights also decrease)
                        noiseHeight += perlinValue * amplitudes[i];
                    }

                    float scaledValue = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, noiseHeight);    // Scale noise height into range of 0.0 - 1.0
                    scaledValue *= heightRatio;         // Scale height using height ratio. This enables terrain height to decrease around rivers
                                                        // and lakes etc.

                    // Create the last octaves (the finest ones). This is done after scaling with heightratio because otherwise the finest surface shapes
                    // would vanish in the case of small height ratio.
                    for (int i = amplitudes.GetLength(0)-numFineOctaves; i < amplitudes.GetLength(0); i++)    // Loop last two items in amplitude and frequency arrays
                    {
                        float sampleX = webMercatorCoord.X / scale * frequencies[i];  // Scale the value (perlin noise gives always same values for integer coordinates)
                        float sampleY = webMercatorCoord.Y / scale * frequencies[i];

                        float perlinValue = Mathf.PerlinNoise(sampleX, sampleY);
                        scaledValue += perlinValue * amplitudes[i];
                    }
                    
                    float baseHeightVal = terrainBaseHeight / terrainMaxHeight;   // Terrain base height
                    scaledValue = baseHeightVal + (1.0f - baseHeightVal) * scaledValue;   // Scale min value to terrain base height and max to 1.0
                    noiseMap[y, x] = scaledValue;

                }
            }
        }

        return noiseMap;
    }


}
