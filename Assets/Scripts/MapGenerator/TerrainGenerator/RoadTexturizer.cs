﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class RoadTexturizer : BaseTexturizer<RasterGeometry> {

    private Rasterizer rasterizer;

    // Constructor
    public RoadTexturizer(TerrainData terrainData, int numLayers, Dictionary<string, Coord> worldTransformedBbox)
        : base(terrainData, numLayers)
    {
        rasterizer = new Rasterizer(terrainData.size.x, terrainData.size.z, terrainData.alphamapResolution, worldTransformedBbox);
    }

    public override float[,,] GenerateAlphamap(List<RasterGeometry> rasterizedGeometries)
    {
        foreach (RasterGeometry linestr in rasterizedGeometries)   // Loop all linestrings
        {
            AlphamapIndices linestrIndices = linestr.GetRaster()[0];

            foreach (Idx idx in linestrIndices.GetIndices() )
            {
                Dictionary<string, string> properties = linestr.GetProperties();

                // The most important/largest ways
                if (properties["highway"].Equals("motorway") || properties["highway"].Equals("trunk") ||
                    properties["highway"].Equals("primary") || properties["highway"].Equals("secondary"))
                {
                    // 7x7 neighborhood
                    alphamapLayers[idx.Y, idx.X - 1, 0] = 1.0f;     // Left
                    alphamapLayers[idx.Y, idx.X - 2, 0] = 1.0f;     // Left
                    alphamapLayers[idx.Y, idx.X - 3, 0] = 1.0f;     // Left
                    alphamapLayers[idx.Y, idx.X + 1, 0] = 1.0f;     // Right
                    alphamapLayers[idx.Y, idx.X + 2, 0] = 1.0f;     // Right
                    alphamapLayers[idx.Y, idx.X + 3, 0] = 1.0f;     // Right
                    alphamapLayers[idx.Y - 1, idx.X, 0] = 1.0f;     // Bottom
                    alphamapLayers[idx.Y - 2, idx.X, 0] = 1.0f;     // Bottom
                    alphamapLayers[idx.Y - 3, idx.X, 0] = 1.0f;     // Bottom
                    alphamapLayers[idx.Y + 1, idx.X, 0] = 1.0f;     // Top
                    alphamapLayers[idx.Y + 2, idx.X, 0] = 1.0f;     // Top
                    alphamapLayers[idx.Y + 3, idx.X, 0] = 1.0f;     // Top
                }

                // Smaller ways
                else if (properties["highway"].Equals("tertiary") || properties["highway"].Equals("unclassified"))
                {
                    // 3x3 neighborhood
                    alphamapLayers[idx.Y, idx.X - 1, 0] = 1.0f;     // Left
                    alphamapLayers[idx.Y, idx.X + 1, 0] = 1.0f;     // Right
                    alphamapLayers[idx.Y - 1, idx.X, 0] = 1.0f;     // Bottom
                    alphamapLayers[idx.Y + 1, idx.X, 0] = 1.0f;     // Top 
                }

                // The least important ways
                else
                {
                    // 3x3 neighborhood
                    alphamapLayers[idx.Y, idx.X - 1, 0] = 1.0f;     // Left
                    alphamapLayers[idx.Y, idx.X + 1, 0] = 1.0f;     // Right
                    alphamapLayers[idx.Y - 1, idx.X, 0] = 1.0f;     // Bottom
                    alphamapLayers[idx.Y + 1, idx.X, 0] = 1.0f;     // Top
                        
                }

                alphamapLayers[idx.Y, idx.X, 0] = 1.0f;
            }
            
        }

        return alphamapLayers;
    }

}
