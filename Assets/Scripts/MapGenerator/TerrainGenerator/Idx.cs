﻿using UnityEngine;

public class Idx
{
    public int X { get; set; }
    public int Y { get; set; }

    public Idx(int x, int y)
    {
        X = x;
        Y = y;
    }
}