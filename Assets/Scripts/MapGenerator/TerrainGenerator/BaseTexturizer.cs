﻿using UnityEngine;
using System.Collections.Generic;

public abstract class BaseTexturizer<T> 
{
    protected float[,,] alphamapLayers;

    // Constructor
    public BaseTexturizer(TerrainData terrainData, int numLayers)
    {
        alphamapLayers = new float[terrainData.alphamapResolution, terrainData.alphamapResolution,
            numLayers];
    }

    abstract public float[,,] GenerateAlphamap(List<T> geometries);
    
}
