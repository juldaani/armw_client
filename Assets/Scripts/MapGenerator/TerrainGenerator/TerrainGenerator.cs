﻿/*
 * TerrainGenerator creates a terrain chunk according to vector geometries fetched from the server.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class TerrainGenerator : MonoBehaviour
{
    public float height;            // Heightmap max height
    private int resolution;          // Height and alphamap resolution (should be 2^n + 1 )
    private int padding;             // Padding around the map block
    public int terrainBaseHeight;   // Base height for terrain
    public float waterHeight;       // Height for water

    public Terrain terrain;
    private TerrainData terrainData;

    public int waterPolygonSmoothingWindowSize; // Window used to smooth edges of polygons containing water. Must be odd number (5x5, 7x7...)
    [Range(0.01f, 4.0f)]
    public float waterPolygonSmoothingSlope;    // Steepness of polygon edges
    [Range(0.0f, 5.0f)]
    public float waterPolygonEdgeRnd;           // Randomness added to water area polygon edges (lakes, riverbanks)

    public int riverWidth;          // River width (pixels in alphamap). Must be odd number (5x5, 7x7...)
    [Range(0.01f, 1.0f)]
    public float riverEdgeSlope;    // Steepness of river edges.
    [Range(0.0f, 5.0f)]
    public float riverEdgeRnd;      // Randomness added to river edges

    // Streams (smaller waterways than rivers)
    public int streamWidth;
    [Range(0.01f, 1.0f)]
    public float streamEdgeSlope;
    [Range(0.0f, 5.0f)]
    public float streamEdgeRnd;

    // Ditches (smaller waterways than streams)
    public int ditchWidth;
    [Range(0.01f, 1.0f)]
    public float ditchEdgeSlope;
    [Range(0.0f, 5.0f)]
    public float ditchEdgeRnd;

    private Dictionary<string, Coord> transformedBbox;     // Bbox of map block (transformed to origin from web mercator coordinates)

    [Range(0.0f, 1.0f)]
    public float treeDensity;   // Defines the density of trees in forests. Must be between 0.0 and 1.0

    RasterGeometries rasterGeometries;

    // Initialize stuffs
    void Start()
    {
        GameObject mapGenerator = GameObject.Find("MapGenerator");
        rasterGeometries = mapGenerator.GetComponent<RasterGeometries>();

        MapGeneratorManager mapGeneratorManager = mapGenerator.GetComponent<MapGeneratorManager>();
        int offset = mapGeneratorManager.GetOffset();
        resolution = mapGeneratorManager.GetResolution();
        padding = mapGeneratorManager.GetPadding();

        // Set terrain material
        terrain.materialType = Terrain.MaterialType.BuiltInStandard;
        //terrain.materialType = Terrain.MaterialType.BuiltInLegacyDiffuse;

        terrainData = new TerrainData();

        // Set heightmap and alphamap
        terrainData.heightmapResolution = resolution;
        terrainData.alphamapResolution = resolution;

        // Set textures
        SplatPrototype grassSplatPrototype = new SplatPrototype();
        grassSplatPrototype.texture = (Texture2D)Resources.Load("Textures/Terrain/Hand_Painted_Grass", typeof(Texture2D));
        grassSplatPrototype.normalMap = (Texture2D)Resources.Load("Textures/Terrain/Hand_Painted_Grass_normal", typeof(Texture2D));
        //grassSplatPrototype.smoothness = 0.2f;
        grassSplatPrototype.tileSize = new Vector2(5, 5);

        SplatPrototype grassSplatPrototype2 = new SplatPrototype();
        grassSplatPrototype2.texture = (Texture2D)Resources.Load("Textures/Terrain/Grass 05/diffuse", typeof(Texture2D));
        grassSplatPrototype2.normalMap = (Texture2D)Resources.Load("Textures/Terrain/Grass 05/normal", typeof(Texture2D));
        grassSplatPrototype2.tileSize = new Vector2(10, 10);

        SplatPrototype sandSplatPrototype = new SplatPrototype();
        sandSplatPrototype.texture = (Texture2D)Resources.Load("Textures/Terrain/ground2/ground2_Diffuse", typeof(Texture2D));
        sandSplatPrototype.normalMap = (Texture2D)Resources.Load("Textures/Terrain/ground2/ground2_Normal", typeof(Texture2D));
        sandSplatPrototype.tileSize = new Vector2(30, 30);

        SplatPrototype sandSplatPrototype2 = new SplatPrototype();
        sandSplatPrototype2.texture = (Texture2D)Resources.Load("Textures/Terrain/Sand 08/diffuse", typeof(Texture2D));
        sandSplatPrototype2.normalMap = (Texture2D)Resources.Load("Textures/Terrain/Sand 08/normal", typeof(Texture2D));
        sandSplatPrototype2.tileSize = new Vector2(15, 18);

        SplatPrototype hillTopSplatPrototype = new SplatPrototype();
        hillTopSplatPrototype.texture = (Texture2D)Resources.Load("Textures/Terrain/Grass 02/diffuse", typeof(Texture2D));
        hillTopSplatPrototype.normalMap = (Texture2D)Resources.Load("Textures//Grass 02/normal", typeof(Texture2D));
        hillTopSplatPrototype.tileSize = new Vector2(10, 10);
        
        SplatPrototype hillSlopeSplatPrototype = new SplatPrototype();
        hillSlopeSplatPrototype.texture = (Texture2D)Resources.Load("Textures/Terrain/ForestFloor2/forest_floor_2", typeof(Texture2D));
        hillSlopeSplatPrototype.normalMap = (Texture2D)Resources.Load("Textures/Terrain/ForestFloor2/forest_floor_2_normal", typeof(Texture2D));
        hillSlopeSplatPrototype.tileSize = new Vector2(10, 10);

        SplatPrototype roadSplatPrototype = new SplatPrototype();
        roadSplatPrototype.texture = (Texture2D)Resources.Load("Textures/Terrain/Ground & moss/diffuse", typeof(Texture2D));
        roadSplatPrototype.normalMap = (Texture2D)Resources.Load("Textures/Terrain/Ground & moss/normal", typeof(Texture2D));
        roadSplatPrototype.tileSize = new Vector2(20, 20);

        SplatPrototype farmlandSplatPrototype = new SplatPrototype();
        farmlandSplatPrototype.texture = (Texture2D)Resources.Load("Textures/Terrain/ground11/ground11_Diffuse", typeof(Texture2D));
        farmlandSplatPrototype.normalMap = (Texture2D)Resources.Load("Textures/Terrain/ground11/ground11_Normal", typeof(Texture2D));
        farmlandSplatPrototype.tileSize = new Vector2(20, 20);

        terrainData.splatPrototypes = new SplatPrototype[] { sandSplatPrototype, sandSplatPrototype2, grassSplatPrototype, grassSplatPrototype2,
            hillTopSplatPrototype, hillSlopeSplatPrototype, roadSplatPrototype, farmlandSplatPrototype};


        // Set water plane size & position
        GameObject waterPlane = GameObject.FindGameObjectWithTag("WaterPlane");
        waterPlane.transform.position = new Vector3(0, waterHeight, 0);
        waterPlane.transform.localScale = new Vector3(offset, 1, offset);

        // Set trees
        TreePrototype tree = new TreePrototype();
        tree.prefab = (GameObject)Resources.Load("Tree10/Tree10_1", typeof(GameObject));
        terrainData.treePrototypes = new TreePrototype[] { tree };

        terrainData.RefreshPrototypes();
    }
    // * * * * * * * * * * * * * * * * * * *
    // END OF Start()


    public Terrain GenerateTerrain(VectorGeometries geometries, int offset, Coord mapBlockGenerationCoordinateWorld)
    {
        // Get bbox
        transformedBbox = geometries.GetTransformedBbox();

        // Generate equal sized map blocks and add some padding
        float lengthX = offset * 2 + padding*2;
        float lengthZ = offset * 2 + padding*2;
        float deltaX = (lengthX - (transformedBbox["max"].X - transformedBbox["min"].X)) / 2;
        float deltaZ = (lengthZ - (transformedBbox["max"].Y - transformedBbox["min"].Y)) / 2;
        
        terrainData.size = new Vector3(lengthX, height, lengthZ);   // Set terrain size

        //System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
        //stopwatch.Start();

        float[,] heightMap = HeightmapGenerator.GenerateHeightMap(resolution, resolution,
            terrainBaseHeight, height, terrainData, geometries, rasterGeometries, offset, mapBlockGenerationCoordinateWorld);

        //stopwatch.Stop();
        //Debug.Log("Time taken: " + (stopwatch.ElapsedMilliseconds));
        //stopwatch.Reset();

        // Create Set heightmap height to 0.0 for water areas (lakes, rivers..)
        WaterGenerator waterGenerator = new WaterGenerator(terrainData, transformedBbox, mapBlockGenerationCoordinateWorld, 
            waterPolygonSmoothingWindowSize, waterPolygonSmoothingSlope, waterPolygonEdgeRnd, 
            riverWidth, riverEdgeSlope, riverEdgeRnd,
            streamWidth, streamEdgeSlope, streamEdgeRnd,
            ditchWidth, ditchEdgeSlope, ditchEdgeRnd);
        heightMap = waterGenerator.MakeHeightmapForWaterAreas(heightMap, geometries);
        
        terrainData.SetHeights(0, 0, heightMap);   // Set heightmap for terrain data

        // Generate textures for the terrain
        float[,,] terrainTextures = GenerateTerrainTextures(terrainData, rasterGeometries, geometries);     // Landscape textures
        terrainData.SetAlphamaps(0, 0, terrainTextures);    // Set alphamap

        // Create forests
        try
        {
            List<TreeInstance> trees = GenerateTrees(geometries);
            terrainData.treeInstances = trees.ToArray();
        }
        catch(KeyNotFoundException e)
        {
            Debug.Log(e);
        }


        // Update terrain
        terrain.GetComponent<TerrainCollider>().terrainData = terrainData;
        terrain.terrainData = terrainData;
        //terrain.transform.position = new Vector3(transformedBbox["min"].X-deltaX, 0, transformedBbox["min"].Y-deltaZ);
        terrain.transform.position = new Vector3(-terrainData.size.x/2, 0, -terrainData.size.z/2);
        //terrain.transform.position = new Vector3(0, 0, 0);

        terrain.Flush();    // Flush changes

        return terrain;
    }
    // * * * * * * * * * * * * * * * * * * *
    // END OF GenerateTerrain()



    /*
     * Generate textures for the terrain.
     */
    private float[,,] GenerateTerrainTextures(TerrainData terrainData, RasterGeometries rasterGeometries, VectorGeometries geometries)
    {
        Dictionary<string, List<string>>[,] grid = rasterGeometries.GetGrid();
        float[,,] tmpSplatmap = new float[terrainData.alphamapResolution, terrainData.alphamapResolution, terrainData.alphamapLayers];

        // Create textures for the landscape based on the terrain height and steepness.
        for (int x = 0; x < terrainData.alphamapResolution; x++)  // Loop through raster grid
        {
            for (int y = 0; y < terrainData.alphamapResolution; y++)
            {
                // Normalise x/y coordinates to range 0-1 
                float y_01 = (float)y / (float)(terrainData.alphamapResolution - 1);
                float x_01 = (float)x / (float)(terrainData.alphamapResolution - 1);

                // Get the height at the current point
                float curHeight = terrainData.GetInterpolatedHeight(x_01, y_01);
                float maxHeight = terrainData.size.y;
                float height_01 = curHeight / maxHeight;    // Height normalized to range 0-1

                // Get the steepness at the current point
                float steepness = terrainData.GetSteepness(x_01, y_01);

                // Get world coordinate for the current noise map (heightmap) index
                Vector3 worldCoord = AlphamapIndices.AlphamapIndexToWorldCoordinate(new Idx(x, y),
                    terrainData.size.x, terrainData.size.z, terrainData.heightmapResolution);

                // Transform the current world coordinate to web mercator (true locations)
                Coord webMercatorCoord = CoordinateConverter.WorldCoordinateToWebMercator(new Coord(worldCoord.x, worldCoord.z),
                    geometries);

                // Get perlin noise value for a given coordinate
                float perlinScaleSand = 300.35f;
                float perlinScaleGrass = 125.35f;   // Scale the value (perlin noise gives always the same values for integer coordinates)
                float sampleX = webMercatorCoord.X;  
                float sampleY = webMercatorCoord.Y;
                float perlinValueSand = Mathf.PerlinNoise(sampleX / perlinScaleSand, sampleY / perlinScaleSand);
                float perlinValueGrass = Mathf.PerlinNoise(sampleX / perlinScaleGrass + 1000.4f, sampleY / perlinScaleGrass);

                // Get texture weights based on terrain height
                float[] tmpSplatWeights = CalculateSplatWeightsBasedOnTerrainHeight(height_01);

                // Sand
                tmpSplatmap[y, x, 0] = tmpSplatWeights[0] * perlinValueSand;
                tmpSplatmap[y, x, 1] = tmpSplatWeights[0] * (1 - perlinValueSand) * 0.8f;

                // Grass
                tmpSplatmap[y, x, 2] = tmpSplatWeights[1] * (1 - perlinValueGrass * 0.13f);
                tmpSplatmap[y, x, 3] = tmpSplatWeights[1] * perlinValueGrass * 1.0f;

                // Top texture
                tmpSplatmap[y, x, 4] = tmpSplatWeights[2];

                // Hill slope texture (steepness)
                tmpSplatmap[y, x, 5] = Mathf.Clamp(steepness * steepness * 0.7f / maxHeight, 0.0f, 4.0f);

                // Farmland
                Dictionary<string, List<string>> curCell = grid[x, y];
                if (curCell != null && curCell.ContainsKey("landuse") )
                {
                    List<string> tag = curCell["landuse"];

                    if (tag != null && tag.Contains("farmland"))
                    {
                        tmpSplatmap[y, x, 7] = 50.6f;   /// Change back to 0.6f
                    }
                }

                // Residential areas
                if (curCell != null && curCell.ContainsKey("landuse"))
                {
                    List<string> tag = curCell["landuse"];

                    if (tag != null && tag.Contains("residential"))
                    {
                        tmpSplatmap[y, x, 1] = 0.8f;
                    }
                }
            }
        }


        // Texturize roads using 3x3 window
        List<RasterGeometry> linestrings = rasterGeometries.GetGeometriesWithTag("highway", "Linestring");
        foreach (RasterGeometry linestr in linestrings)   // Loop all linestrings
        {
            AlphamapIndices linestrIndices = linestr.GetRaster()[0];

            foreach (Idx idx in linestrIndices.GetIndices())
            {
                float val = 3.0f;
                tmpSplatmap[idx.Y, idx.X, 6] = val;         // Center
                tmpSplatmap[idx.Y, idx.X - 1, 6] = val;     // Left
                tmpSplatmap[idx.Y, idx.X + 1, 6] = val;     // Right
                tmpSplatmap[idx.Y - 1, idx.X, 6] = val;     // Bottom
                tmpSplatmap[idx.Y + 1, idx.X, 6] = val;     // Top 
            }
        }


        // Normalize
        for (int x = 0; x < terrainData.alphamapResolution; x++)  // Loop through raster grid
        {
            for (int y = 0; y < terrainData.alphamapResolution; y++)
            {
                // Sum of all textures weights must add to 1, so calculate normalization factor from sum of weights
                float z = 0.0f;
                for (int i = 0; i < terrainData.alphamapLayers; i++)
                {
                    z += tmpSplatmap[x, y, i];
                }

                // Loop through each terrain texture layer
                for (int i = 0; i < terrainData.alphamapLayers; i++)
                {
                    // Normalize so that sum of all texture weights = 1
                    tmpSplatmap[x, y, i] /= z;
                }
            }
        }

       return tmpSplatmap;

    }
    // * * * * * * * * * * * * * * * * * * *
    // END OF GenerateTerrainTextures()


    /*
     * Calculate splat weights based on the height of the terrain (3 different textures).
     */
    private float[] CalculateSplatWeightsBasedOnTerrainHeight(float height)
    {
        // 0 sand, 1 grass, 2 top
        float[] splatWeights = new float[3];

        // Upper limits for the terrain height values
        float sandLimit = 0.09f;
        float grassLimit = 0.60f;
        float overlap = 0.45f;
        float overlapTop = 0.32f;

        // Sand
        if (height < sandLimit)
        {
            splatWeights[0] = 1.0f;     // Sand
            splatWeights[1] = 0.0f;     // Grass
            splatWeights[2] = 0.0f;     // Top
        }
        // Transition zone between sand and grass
        else if ( (height >= sandLimit) && (height <= sandLimit + overlap) )
        {
            float tmpVal = (height - sandLimit) * (1 / overlap);
            splatWeights[0] = 1.0f - tmpVal;
            splatWeights[1] = tmpVal;
            splatWeights[2] = 0.0f;
        }
        // Grass
        else if ( (height > sandLimit + overlap) && (height < grassLimit) )
        {
            splatWeights[0] = 0.0f;
            splatWeights[1] = 1.0f;
            splatWeights[2] = 0.0f;
        }
        // Transition zone between grass and top
        else if ( (height >= grassLimit) && (height <= grassLimit + overlapTop))
        {
            float tmpVal = (height - grassLimit) * (1 / overlapTop);
            splatWeights[0] = 0.0f;
            splatWeights[1] = 1.0f - tmpVal;
            splatWeights[2] = tmpVal;
        }
        // Top
        else if ( height > grassLimit + overlapTop)
        {
            splatWeights[0] = 0.0f;
            splatWeights[1] = 0.0f;
            splatWeights[2] = 1.0f;
        }

        return splatWeights;
    }
    // * * * * * * * * * * * * * * * * * * *
    // END OF CalculateSplatWeightsBasedOnTerrainHeight()


    /*
     * Generate trees based on forest polygons.
     */
    private List<TreeInstance> GenerateTrees(VectorGeometries geometries)
    {
        List<TreeInstance> trees = new List<TreeInstance>();

        List<RasterGeometry> forestPolygs = rasterGeometries.GetGeometriesWithTagAndProperty("landuse", "forest", "Polygon");
        forestPolygs.AddRange(rasterGeometries.GetGeometriesWithTagAndProperty("natural", "wood", "Polygon"));
        int resolution = terrainData.alphamapResolution;

        foreach (RasterGeometry polyg in forestPolygs)
        {
            AlphamapIndices polygonArea = polyg.GetRaster()[0];

            foreach (Idx idx in polygonArea.GetIndices() )
            {
                float treePosX = (float)idx.X / (resolution-1);
                float treePosY = (float)idx.Y / (resolution-1);

                if ( Random.Range(0.0f, 1.0f) < treeDensity) {

                    TreeInstance treeInstance = new TreeInstance();

                    treePosX += Random.Range(-0.003f, 0.003f);
                    treePosY += Random.Range(-0.003f, 0.003f);

                    float altitude = terrainData.GetInterpolatedHeight(treePosX, treePosY);

                    treeInstance.position = new Vector3(treePosX, altitude/height, treePosY);

                    treeInstance.widthScale = 1 + Random.Range(-0.2f, 0.2f);
                    treeInstance.heightScale = 1 + Random.Range(-0.3f, 1.0f);

                    treeInstance.rotation = Random.Range(0.0f, 6.28f);
                    treeInstance.color = Color.white;
                    treeInstance.lightmapColor = Color.white;
                    treeInstance.prototypeIndex = 0;

                    trees.Add(treeInstance);
                }

            }
        }

        return trees;
    }
    // * * * * * * * * * * * * * * * * * * *
    // END OF GenerateTrees()


    private float[,,] MakeAlphamap(TerrainData terrainData, VectorGeometries geometries)
    {
        float[,,] splatmapLayerRoad = null;
        float[,,] splatmapLayerLanduse = null;

        // Highway textures
        RoadTexturizer roadTexturizer = new RoadTexturizer(terrainData, 1, transformedBbox);
        List<RasterGeometry> linestrings = rasterGeometries.GetGeometriesWithTag("highway", "Linestring");
        splatmapLayerRoad = roadTexturizer.GenerateAlphamap(linestrings);

        // Landuse textures
        LanduseTexturizer landuseTexturizer = new LanduseTexturizer(terrainData, 1, transformedBbox);
        List<RasterGeometry> polygs = rasterGeometries.GetGeometriesWithTag("landuse", "Polygon");
        splatmapLayerLanduse = landuseTexturizer.GenerateAlphamap(polygs);


        // Combine layers
        float[,,] tmpSplatmap = new float[terrainData.alphamapResolution, terrainData.alphamapResolution,
            terrainData.alphamapLayers];

        for (int x = 0; x < tmpSplatmap.GetLength(0); x++)  // Loop through splatmap
        {
            for (int y = 0; y < tmpSplatmap.GetLength(1); y++)
            {
                float valueLanduse = 0;
                if (splatmapLayerLanduse != null)
                {
                    valueLanduse = splatmapLayerLanduse[x, y, 0];
                }

                float valueRoad = 0;
                if (splatmapLayerRoad != null)
                {
                    valueRoad = splatmapLayerRoad[x, y, 0];
                }


                // Define priority for different layers: 1. roads, 2. water, 3. landuse, 4. ground
                tmpSplatmap[x, y, 2] = valueRoad;       // Road

                if (valueLanduse > (1 - valueRoad))     // Landuse
                {
                    tmpSplatmap[x, y, 1] = 1 - valueRoad;
                }
                else
                {
                    tmpSplatmap[x, y, 1] = valueLanduse;
                }

                if ((1 - valueRoad - valueLanduse) < 0) // Ground
                {
                    tmpSplatmap[x, y, 0] = 0;
                }
                else
                {
                    tmpSplatmap[x, y, 0] = 1 - valueRoad - valueLanduse;
                }

            }
        }

        return tmpSplatmap;
    }
    // * * * * * * * * * * * * * * * * * * *
    // END OF MakeAlphamap()



    // Getters & setters
    public float GetTerrainBaseHeight()
    {
        return terrainBaseHeight;
    }

    public float GetWaterHeight()
    {
        return waterHeight;
    }

    public float GetTerrainMaxHeight()
    {
        return height;
    }
}
