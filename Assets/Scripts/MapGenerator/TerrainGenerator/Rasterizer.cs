﻿/* Rasterizer is used to rasterize single geometry (polygon, linestring) with respect to the
 * grid in the alphamap (see Unity Terrain Engine). 
 * 
 * - For polygons, rasterizer can be used to either fill the whole polygon or rasterize only the 
 *   polygon edges. 
 * - Resolution of the output is the same as alphamap resolution (TerrainData).
 * - Rasterizer returns a list of alphamap indices in which the geometry crosses corresponding alphamap cells.
 */

using UnityEngine;
using System.Collections.Generic;
using System;


public class Rasterizer
{
    
    private float worldLengthX, worldLengthZ;
    private int resolution;
    private float worldUnitSizeX, worldUnitSizeZ;
    
    //private TerrainData terrainData;
    
    private Coord worldTransformedBboxMin;

    Dictionary<string, Coord> worldTransformedBbox;


    /*
     * Constructor
     */
    public Rasterizer(float worldLengthX, float worldLengthZ, int resolution, Dictionary<string, Coord> worldTransformedBbox)
    {
        //this.terrainData = terrainData;
        this.worldTransformedBbox = worldTransformedBbox;
        this.worldLengthX = worldLengthX;
        this.worldLengthZ = worldLengthZ;
        this.resolution = resolution;

        // Corrections because of padding around the map block
        //float moveX = (worldLengthX + (worldTransformedBbox["max"].X - worldTransformedBbox["min"].X)) / 2;
        //float moveZ = (worldLengthZ + (worldTransformedBbox["max"].Y - worldTransformedBbox["min"].Y)) / 2;
        float moveX = worldLengthX / 2;
        float moveZ = worldLengthZ / 2;
        worldTransformedBboxMin = new Coord(worldTransformedBbox["min"].X + moveX, worldTransformedBbox["min"].Y + moveZ);


        worldUnitSizeX = worldLengthX / resolution;
        worldUnitSizeZ = worldLengthZ / resolution;
    }


    /*
     * Rasterize a geometry (linestring or polygon, points are not rasterized)
     */
    public List<AlphamapIndices> Rasterize<T>(List<T> geoms, bool fillPolygons) where T : Geometry
    {
        List<AlphamapIndices> indicesList = new List<AlphamapIndices>();

        foreach (Geometry geom in geoms)
        {
            List<AlphamapIndices> alphamapIdxList = new List<AlphamapIndices>();
            List<List<Coord>> coords = geom.getTransformedCoordinates();
            AlphamapIndices alphamapIndicesOfGeometry = new AlphamapIndices();

            // Rasterize the polygon (fill also inner parts)
            if (geom.GetType() == typeof(Polygon) & fillPolygons)  
            {
                alphamapIndicesOfGeometry = FillPolygon(geom);
            }

            // Rasterize only lines of a geometry (polygons are not filled)
            else
            {
                foreach (List<Coord> coordList in coords)   // Multiple lists if there is interior rings in a polygon
                {
                    Coord prevCoord = null;
                    AlphamapIndices alphamapIndices = new AlphamapIndices();

                    foreach (Coord curCoord in coordList)
                    {
                        if (prevCoord == null)  // If first round
                        {
                            prevCoord = curCoord;
                        }
                        else
                        {
                            Idx curIdx = AlphamapIndices.WorldCoordinateToAlphamapIndex(curCoord, worldTransformedBbox, worldLengthX, 
                                worldLengthZ, resolution);
                            Idx prevIdx = AlphamapIndices.WorldCoordinateToAlphamapIndex(prevCoord, worldTransformedBbox, worldLengthX,
                                worldLengthZ, resolution);

                            alphamapIndices = RasterizeLine(prevIdx, curIdx, alphamapIndices);  // Rasterize single line

                            prevCoord = curCoord;
                        }
                    }

                    alphamapIdxList.Add(alphamapIndices);
                }

                // Copy all indices into a single list
                foreach(AlphamapIndices alphamapIndex in alphamapIdxList)
                {
                    foreach(Idx idx in alphamapIndex.GetIndices())
                    {
                        alphamapIndicesOfGeometry.AddIndex(idx);
                    }
                }

            }

            indicesList.Add(alphamapIndicesOfGeometry);
        }

        return indicesList;
    }
    // * * * END OF Rasterize * * *


    /*
     * Method to rasterize interior parts of a polygon
     */
    public AlphamapIndices FillPolygon(Geometry geom)
    {
        AlphamapIndices indices = new AlphamapIndices();

        Dictionary<string, Coord> bbox = geom.GetTransformedBbox();

        Idx minIndices = AlphamapIndices.WorldCoordinateToAlphamapIndex(bbox["min"], worldTransformedBbox, worldLengthX, 
            worldLengthZ, resolution);
        Idx maxIndices = AlphamapIndices.WorldCoordinateToAlphamapIndex(bbox["max"], worldTransformedBbox, worldLengthX,
            worldLengthZ, resolution);

        // Construct a grid which size is equal to the polygon bounding box
        Coord[,] grid = new Coord[maxIndices.X - minIndices.X + 1, maxIndices.Y - minIndices.Y + 1];

        // List to store polygons edges indexed in y-direction (indexing accelerates rasterizing)
        List<List<Coord[]>> indexedPolygonEdges = new List<List<Coord[]>>();


        // Generate test points and index polygon edges
        for (int y = 0; y < grid.GetLength(1); y++)     // Loop cells in the grid
        {
            for (int x = 0; x < grid.GetLength(0); x++)
            {
                // Generate test points 
                /*
                Coord testPoint = new Coord(
                    (minIndices.X * worldUnitSizeX - (worldUnitSizeX / 2) + worldUnitSizeX * x) 
                        - Math.Abs(worldTransformedBboxMin.X),
                    (minIndices.Y * worldUnitSizeZ - (worldUnitSizeZ / 2) + worldUnitSizeZ * y) 
                        - Math.Abs(worldTransformedBboxMin.Y));
                        */
               /*         
                Coord testPoint = new Coord(
                    (minIndices.X * worldUnitSizeX + (worldUnitSizeX / 2) + worldUnitSizeX * x) - (worldLengthX / 2),
                    (minIndices.Y * worldUnitSizeZ + (worldUnitSizeZ / 2) + worldUnitSizeZ * y) - (worldLengthZ / 2) );
                    */

                Vector3 tmpTestPoint = AlphamapIndices.AlphamapIndexToWorldCoordinate(new Idx(minIndices.X + x, minIndices.Y + y), 
                    worldLengthX, worldLengthZ, resolution);
                Coord testPoint = new Coord(tmpTestPoint.x, tmpTestPoint.z);

                grid[x, y] = testPoint;
            }

            // Index polygon edges according to test point's y-coordinate
            float testPointY = grid[0, y].Y;    // Pick world y-coordinate of a row in the grid
            indexedPolygonEdges.Add(new List<Coord[]>() );

            foreach (List<Coord> coordList in geom.getTransformedCoordinates() )      // Loop outer and inner rings of a polygon
            {
                for (int i = 1; i < coordList.Count; i++)    // Loop coordinates in the ring
                {
                    // If the current polygon edge is crossing test point's y-coordinate
                    if ((coordList[i-1].Y > testPointY) != (coordList[i].Y > testPointY) )
                    {
                        Coord edgeFirstCoord = new Coord(coordList[i - 1].X, coordList[i - 1].Y);
                        Coord edgeLastCoord = new Coord(coordList[i].X, coordList[i].Y);
                        Coord[] polygonEdge = { edgeFirstCoord, edgeLastCoord };

                        indexedPolygonEdges[y].Add(polygonEdge);
                    }
                }
            }
        }


        // Test all cells in the grid with "line scan algorithm" whether they are located inside the polygon or not 
        // (rasterize)
        for (int x = 0; x < grid.GetLength(0); x++)
        {
            for (int y = 0; y < grid.GetLength(1); y++)
            {
                Coord testPoint = grid[x, y];

                List<bool> boolList = new List<bool>();
                List<Coord[]> edges = indexedPolygonEdges[y];

                // Boolean values which tell if the point is inside polygon's outer ring but "outside" of the inner rings
                boolList.Add(IsPointInPolygon(testPoint, edges));

                int counter = 0; bool flag = false;
                foreach (bool isInsideRing in boolList)     // Loop all values in boolList
                {
                    if ((counter == 0) && isInsideRing)     // If the test point is inside the outer ring
                    {
                        flag = true;
                    }

                    if ((counter == 0) && !isInsideRing)   // If the test point is ooutside the outer ring
                    {
                        break;
                    }

                    if ((counter > 0) && isInsideRing)  // If the test point is inside an inner ring
                    {
                        flag = false;
                    }

                    counter++;
                }

                // If the tested point is inside the outer ring but not inside any of the inner rings
                if (flag)
                {
                    indices.AddIndex(new Idx(x + minIndices.X - 1, y + minIndices.Y - 1));
                }
            }
        }

        return indices;
    }
    // * * * END OF FillPolygon * * *


    /*
     * Line scan algorithm to rasterize polygons
     */
    public bool IsPointInPolygon(Coord p, List<Coord[]> edges)
    {
        // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
        bool inside = false;

        foreach(Coord[] edge in edges)
        {
            if ((edge[1].Y > p.Y) != (edge[0].Y > p.Y) &&
                p.X < (edge[0].X - edge[1].X) * (p.Y - edge[1].Y) / (edge[0].Y - edge[1].Y) + edge[1].X)
            {
                inside = !inside;
            }
        }

        return inside;
    }
    
    
    /* 
    * This function is stolen from:
    * https://github.com/SebLague/Procedural-Cave-Generation/blob/master/Episode%2008/MapGenerator.cs
    * The function rasterizes single line segment
    */
    private AlphamapIndices RasterizeLine(Idx from, Idx to,
        AlphamapIndices alphamapIndices)
    {
        int x = from.X;
        int y = from.Y;

        int dx = to.X - from.X;
        int dy = to.Y - from.Y;

        bool inverted = false;
        int step = Math.Sign(dx);
        int gradientStep = Math.Sign(dy);

        int longest = Mathf.Abs(dx);
        int shortest = Mathf.Abs(dy);

        if (longest < shortest)
        {
            inverted = true;
            longest = Mathf.Abs(dy);
            shortest = Mathf.Abs(dx);

            step = Math.Sign(dy);
            gradientStep = Math.Sign(dx);
        }

        int gradientAccumulation = longest / 2;
        for (int i = 0; i < longest; i++)
        {
            alphamapIndices.AddIndex(new Idx(x, y));

            if (inverted)
            {
                y += step;
            }
            else
            {
                x += step;
            }

            gradientAccumulation += shortest;
            if (gradientAccumulation >= longest)
            {
                if (inverted)
                {
                    x += gradientStep;
                }
                else
                {
                    y += gradientStep;
                }
                gradientAccumulation -= longest;
            }
        }

        return alphamapIndices;
    }
    // * * * END OF RasterizeLine * * * 

    
}