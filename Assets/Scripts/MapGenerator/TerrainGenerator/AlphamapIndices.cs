﻿/*
 * Container class which stores indexes (as a list) refering to the alphamap (see Unity Terrain Engine). 
 */

using UnityEngine;
using System.Collections.Generic;

public class AlphamapIndices
{
    private List<Idx> indices;

    // Constructor
    public AlphamapIndices()
    {
        indices = new List<Idx>();
    }

    
    // Convert alphamap index to world coordinate
    public static Vector3 AlphamapIndexToWorldCoordinate(Idx idx, float worldLengthX, float worldLengthZ, int resolution)
    {
        // Move left corner of the map block to origo
        float moveX = worldLengthX / 2;
        float moveZ = worldLengthZ / 2;

        float worldUnitSizeX = worldLengthX / resolution;
        float worldUnitSizeZ = worldLengthZ / resolution;

        float xWorldCoord =  (idx.X * worldUnitSizeX) - worldUnitSizeX / 2 - moveX;
        float zWorldCoord =  (idx.Y * worldUnitSizeZ) - worldUnitSizeZ / 2 - moveZ;

        return new Vector3(xWorldCoord, 0, zWorldCoord);
    }
    

    // Convert world coordinate point into alphamap index (can also be used for heightmaps and splatmaps)
    public static Idx WorldCoordinateToAlphamapIndex(Coord worldCoordinatePoint, Dictionary<string, Coord> transformedBbox,
        float worldLengthX, float worldLengthZ, int resolution)
    {
        // Move left corner of the map block to origo
        float moveX = worldLengthX / 2;
        float moveZ = worldLengthZ / 2;
        Coord tmpTransformedBboxMin = new Coord(transformedBbox["min"].X + moveX, transformedBbox["min"].Y + moveZ);

        float worldUnitSizeX = worldLengthX / resolution;
        float worldUnitSizeZ = worldLengthZ / resolution;

        // Move coordinate point according to moveX and moveZ variables (to ease the calculations because indexes
        // are not negative).
        float newX = worldCoordinatePoint.X + moveX;
        float newY = worldCoordinatePoint.Y + moveZ;


        // Calculate in which alphamap cell the point is lying. 
        int idx_X = Mathf.FloorToInt(newX / worldUnitSizeX);
        int idx_Y = Mathf.FloorToInt(newY / worldUnitSizeZ);

        // Prevent index going out of range if the point lies exactly on the edge of the map block
        if (idx_X == resolution)
        {
            idx_X = resolution - 1;
        }
        if (idx_Y == resolution)
        {
            idx_Y = resolution - 1;
        }

        return new Idx(idx_X, idx_Y);
    }


    // Convert a list of alphamap indices to 2D-array
    public Idx[,] IndicesToArray()
    {
        Idx minIdx = FindMin();
        Idx maxIdx = FindMax();

        // Calculate array dimensions
        int numCellsX = maxIdx.X - minIdx.X + 1;
        int numCellsY = maxIdx.Y - minIdx.Y + 1;

        Idx[,] grid = new Idx[numCellsX, numCellsY];

        // Generate an array
        foreach(Idx idx in indices)
        {
            grid[idx.X - minIdx.X, idx.Y - minIdx.Y] = idx;
        }

        return grid;
    }
    // * * * END OF AlphamapIndicesToArray * * *


    // Find minimum value of indices
    public Idx FindMin()
    {
        int minX = -1, minY = -1;
        foreach (Idx idx in indices)
        {
            if (minX == -1)  // If first round
            {
                minX = idx.X; minY = idx.Y;
            }
            else
            {
                if (idx.X < minX)
                    minX = idx.X;
                if (idx.Y < minY)
                    minY = idx.Y;
            }
        }

        return new Idx(minX, minY);
    }


    // Find maximum value of indices
    public Idx FindMax()
    {
        int maxX = -1, maxY = -1;
        foreach (Idx idx in indices)
        {
            if (maxX == -1)  // If first round
            {
                maxX = idx.X; maxY = idx.Y;
            }
            else
            {
                if (idx.X > maxX)
                    maxX = idx.X;
                if (idx.Y > maxY)
                    maxY = idx.Y;
            }
        }

        return new Idx(maxX, maxY);
    }


    // Getters & setters
    public List<Idx> GetIndices()
    {
        return indices;
    }

    public void SetIndices(List<Idx> indices)
    {
        this.indices = indices;
    }

    public void SetIndices(Idx[,] idxArray)
    {
        indices.Clear();

        for (int x = 0; x < idxArray.GetLength(0); x++)
        {
            for (int y = 0; y < idxArray.GetLength(1); y++)
            {
                if(idxArray[x,y] != null)
                {
                    indices.Add(idxArray[x, y]);
                }
            }
        }
    }

    public void AddIndex(Idx index)
    {
        indices.Add(index);
    }

}