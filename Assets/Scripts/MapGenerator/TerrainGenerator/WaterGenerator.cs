﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterGenerator {

    private Rasterizer rasterizer;

    private int waterPolygonSmoothingWindowSize;
    private float waterPolygonSmoothingSlope;
    private float waterPolygonEdgeRnd;

    private int riverWidth;
    private float riverEdgeSlope;
    private float riverEdgeRnd;

    private int streamWidth;
    private float streamEdgeSlope;
    private float streamEdgeRnd;

    private int ditchWidth;
    private float ditchEdgeSlope;
    private float ditchEdgeRnd;

    private float terrainBaseHeight;
    private float waterHeight;
    private float terrainMaxHeight;

    private TerrainData terrainData;

    private Coord mapBlockGenerationCoordinateWorld;
    private int offset;

    RasterGeometries rasterGeometries;

    // Constructor
    public WaterGenerator(
        TerrainData terrainData, Dictionary<string, Coord> worldTransformedBbox, Coord mapBlockGenerationCoordinateWorld,
        int waterPolygonSmoothingWindowSize, float waterPolygonSmoothingSlope, float waterPolygonEdgeRnd,
        int riverWidth, float riverEdgeSlope, float riverEdgeRnd,
        int streamWidth, float streamEdgeSlope, float streamEdgeRnd,
        int ditchWidth, float ditchEdgeSlope, float ditchEdgeRnd)
    {
        GameObject mapGenerator = GameObject.Find("MapGenerator");
        rasterGeometries = mapGenerator.GetComponent<RasterGeometries>();
        MapGeneratorManager mapGeneratorManager = mapGenerator.GetComponent<MapGeneratorManager>();
        offset = mapGeneratorManager.GetOffset();

        rasterizer = new Rasterizer(terrainData.size.x, terrainData.size.z, terrainData.alphamapResolution, worldTransformedBbox);

        this.terrainData = terrainData;
        this.mapBlockGenerationCoordinateWorld = mapBlockGenerationCoordinateWorld;

        this.waterPolygonSmoothingWindowSize = waterPolygonSmoothingWindowSize;
        this.waterPolygonSmoothingSlope = waterPolygonSmoothingSlope;
        this.waterPolygonEdgeRnd = waterPolygonEdgeRnd;

        this.riverWidth = riverWidth;
        this.riverEdgeSlope = riverEdgeSlope;
        this.riverEdgeRnd = riverEdgeRnd;

        this.streamWidth = streamWidth;
        this.streamEdgeSlope = streamEdgeSlope;
        this.streamEdgeRnd = streamEdgeRnd;

        this.ditchWidth = ditchWidth;
        this.ditchEdgeSlope = ditchEdgeSlope;
        this.ditchEdgeRnd = ditchEdgeRnd;

        // Get terrain and water heights
        TerrainGenerator terrainGenerator = GameObject.Find("MapGenerator").GetComponent<TerrainGenerator>();
        terrainBaseHeight = terrainGenerator.GetTerrainBaseHeight();
        waterHeight = terrainGenerator.GetWaterHeight();
        terrainMaxHeight = terrainGenerator.GetTerrainMaxHeight();
    }
    // * * * * * * * * END OF WaterGenerator * * * * * * * * * 



    /*
     * Function sets heightmap heights to 0 for areas filled with water.
     * 
     * - Polygons (lakes, riverbanks..) are first "filled" and then polygon edges are smoothed.
     * - Linestrings (rivers, ) are handled by creating a "groove" on the terrain along the linestrings. Edges of the groove are also smoothed.
     * 
     * "waterPolygonSmoothingWindowSize" must be odd number like (5x5, 7x7...)
     */
    public float[,] MakeHeightmapForWaterAreas(float[,] heightmap, VectorGeometries geometries)
    {

        // Create lakes and riverbanks (polygons)
        // * * * * * * * * * * * * * * * * * * * * * *
        List<RasterGeometry> lakePolygs = rasterGeometries.GetGeometriesWithTagAndProperty("natural", "water", "Polygon");  // Get polygons for lakes
        List<RasterGeometry> riverbankPolygs = rasterGeometries.GetGeometriesWithTagAndProperty("waterway", "riverbank", "Polygon");    // Get polygons for riverbanks

        // Combine lists
        List<RasterGeometry> waterPolygs = new List<RasterGeometry>();
        waterPolygs.AddRange(riverbankPolygs);
        waterPolygs.AddRange(lakePolygs);

        // "Fill" polygons
        List<AlphamapIndices> boundaryIndices = new List<AlphamapIndices>();
        foreach (RasterGeometry rasterizedPolyg in waterPolygs)
        {
            AlphamapIndices polygonIndices = rasterizedPolyg.GetRaster()[0];    // Polygon areas
            foreach (Idx idx in polygonIndices.GetIndices() )
            {
                heightmap[idx.Y, idx.X] = 0.0f;
            }

            polygonIndices = rasterizedPolyg.GetRaster()[1];    // Polygon boundaries
            boundaryIndices.Add(polygonIndices);
        }

        ApplyPolygonEdgeSmoothingWindow(ref heightmap, boundaryIndices);    // Smooth polygon edges using NxN window


        // Create rivers, canals, streams and ditches (linestrings)
        // * * * * * * * * * * * * * * * * * * * * * *
        // Rivers and canals
        List<RasterGeometry> riverCanalLinestrings = rasterGeometries.GetGeometriesWithTagAndProperty("waterway", "river", "Linestring");    // Get linestrings for rivers
        riverCanalLinestrings.AddRange(rasterGeometries.GetGeometriesWithTagAndProperty("waterway", "canal", "Linestring"));  // Get linestrings for canals
        List<AlphamapIndices> indices = RasterGeometries.GetAlphamapIndices(riverCanalLinestrings, false);
        ApplyRiverWindow(ref heightmap, indices, riverWidth, riverEdgeSlope, riverEdgeRnd); // Apply window (set heightmap heights according to window)

        // Streams
        List<RasterGeometry> streamLinestrings = rasterGeometries.GetGeometriesWithTagAndProperty("waterway", "stream", "Linestring");  // Get linestrings for streams
        indices = RasterGeometries.GetAlphamapIndices(streamLinestrings, false);
        ApplyRiverWindow(ref heightmap, indices, streamWidth, streamEdgeSlope, streamEdgeRnd);

        // Ditches
        List<RasterGeometry> ditchLinestrings = rasterGeometries.GetGeometriesWithTagAndProperty("waterway", "ditch", "Linestring");  // Get linestrings for streams
        indices = RasterGeometries.GetAlphamapIndices(ditchLinestrings, false);
        ApplyRiverWindow(ref heightmap, indices, ditchWidth, ditchEdgeSlope, ditchEdgeRnd);


        // Create coastlines
        // * * * * * * * * * * * * * * * * * * * * * *
        // Get geometries for coastlines
        List<Geometry> coastLinestrings = geometries.GetGeometriesWithTagAndProperty("natural", "coastline", "Linestring");
        List<Geometry> coastPolygons = geometries.GetGeometriesWithTagAndProperty("natural", "coastline", "Polygon");

        // Coastline polygons should be reversed because their coordinates should be running counterclockwise (water on the right hand side).
        foreach (Geometry geom in coastPolygons)     // Loop coastline polygons 
        {
            geom.getTransformedCoordinates()[0].Reverse();
        }

        // Combine lists
        List<Geometry> coastlines = new List<Geometry>();
        coastlines.AddRange(coastLinestrings);
        coastlines.AddRange(coastPolygons);

        CreateCoastlines(ref heightmap, coastlines, geometries);


        return heightmap;
    }
    // * * * * * * * * END OF MakeHeightmapForWaterAreas() * * * * * * * * * 



    /*
     * Method generates water areas according to coastlines.
     */
    private void CreateCoastlines(ref float[,] heightmap, List<Geometry> coastlines, VectorGeometries geometries)
    {
        Dictionary<string, Coord> transformedBbox = geometries.GetTransformedBbox();
        int resolution = terrainData.heightmapResolution;
        float worldUnitSize = terrainData.size.x / resolution;

        // Get alphamap indices for the map block generation coordinate and offset
        Idx mapBlockGenerationWorldCoordinateIdx = AlphamapIndices.WorldCoordinateToAlphamapIndex(mapBlockGenerationCoordinateWorld,
            transformedBbox, terrainData.size.x, terrainData.size.z, terrainData.alphamapResolution);
        float offsetInHeightmapIdx = Mathf.Floor((offset / worldUnitSize) - 1);    // Shrink little bit to avoid flooding water areas out to the land

        List<AlphamapIndices> indices = rasterizer.Rasterize(coastlines, false);    // Rasterize coastlines

        // Coastline map contains information which heightmap pixels belong to the coastline boundary 
        bool[,] coastlineMap = new bool[heightmap.GetLength(0), heightmap.GetLength(1)];

        // Set coastline pixels in the coastline map to true
        foreach (AlphamapIndices alphamapIndices in indices)
        {
            foreach (Idx idx in alphamapIndices.GetIndices())
            {
                coastlineMap[idx.Y, idx.X] = true;
            }
        }

        // Calculate the number of nodes in all coastlines belonging to the current map block. This is used for adjusting
        // the seed point generation probability.
        int numCoastlineNodes = 0;
        foreach (Geometry geom in coastlines)     // Loop coastlines 
        {
            List<Coord> coastlineCoords = geom.getTransformedCoordinates()[0]; 
            numCoastlineNodes = numCoastlineNodes + coastlineCoords.Count;
        } 


        // Generate water areas according to coastlines
        foreach (Geometry geom in coastlines)     // Loop coastlines 
        {
            List<Coord> coastlineCoords = geom.getTransformedCoordinates()[0];

            for (int i = 1; i < coastlineCoords.Count; i++)   // Loop coordinates in the geometry
            {
                // Adjust the probability to generate a seed point for the line segment. This is done to reduce computational burden. 
                // It is not mandatory to generate seed point for every line segment in the coastline.
                float prob = 30.0f/numCoastlineNodes;
                float rnd = Random.Range(0.0f, 1.0f);

                if (rnd < prob)
                {
                    // Pick one line segment
                    Coord firstPoint = coastlineCoords[i - 1];
                    Coord lastPoint = coastlineCoords[i];

                    // Calculate normal vector for that line segment
                    float dx = lastPoint.X - firstPoint.X;
                    float dy = lastPoint.Y - firstPoint.Y;
                    Vector2 unitNormal = (new Vector2(dy, -dx)).normalized;      // Right hand side (water)

                    // Generate "seed" point in the middle of the line segment
                    Vector2 lineMiddlePoint = new Vector2((lastPoint.X + firstPoint.X) / 2, (lastPoint.Y + firstPoint.Y) / 2);
                    Vector2 normalVec = unitNormal * worldUnitSize;
                    Vector2 seedPoint = lineMiddlePoint + normalVec;
                    Coord tmpSeedPoint = new Coord(seedPoint.x, seedPoint.y);   // Stupid but WorldCoordinateToAlphamapIndex function requires Coord object 

                    // Get alphamap indices for the seed point, map block generation coordinate and offset
                    Idx seedPointIdx = AlphamapIndices.WorldCoordinateToAlphamapIndex(tmpSeedPoint, transformedBbox, terrainData.size.x, 
                        terrainData.size.z, terrainData.alphamapResolution);

                    // Move "scan lines" along the directions of the coordinate axes. "Scan lines" are used to fill water areas according to coastlines.
                    for (int direction = 0; direction < 4; direction++)     // Go through all 4 directions along the coordinate axes
                    {
                        Idx curIdx = new Idx(seedPointIdx.X, seedPointIdx.Y);

                        float squareDist = 0;
                        while (true)
                        {
                            // Distance between the recent map block generation coordinate (as an alphamap index) and the current index in the heightmap.
                            squareDist = Mathf.Pow(curIdx.X - mapBlockGenerationWorldCoordinateIdx.X, 2) +
                                Mathf.Pow(curIdx.Y - mapBlockGenerationWorldCoordinateIdx.Y, 2);

                            // Break the loop when the current index
                            //   - is out of the offset boundary
                            //   - is out of the heightmap index bounds
                            //   - reaches the coastline boundary
                            if ((squareDist > Mathf.Pow(offsetInHeightmapIdx, 2)) ||
                                (curIdx.X < 0) || (curIdx.Y < 0) || (curIdx.X > resolution - 1) || (curIdx.Y > resolution - 1) ||
                                (coastlineMap[curIdx.Y, curIdx.X] == true))
                            {
                                break;
                            }


                            // Initialize the "scan line" indices and booleans for the while loop stop condition
                            Idx scanlinePositiveY = new Idx(curIdx.X, curIdx.Y);
                            Idx scanlineNegativeY = new Idx(curIdx.X, curIdx.Y);
                            Idx scanlinePositiveX = new Idx(curIdx.X, curIdx.Y);
                            Idx scanlineNegativeX = new Idx(curIdx.X, curIdx.Y);
                            bool continueIncrementPositiveX = false, continueIncrementPositiveY = false, continueIncrementNegativeX = false,
                                continueIncrementNegativeY = false;

                            // Avoid useless calculations
                            if (direction == 0 || direction == 1)
                            {
                                continueIncrementPositiveY = true; continueIncrementNegativeY = true;
                            }
                            if (direction == 2 || direction == 3)
                            {
                                continueIncrementPositiveX = true; continueIncrementNegativeX = true;
                            }

                            // Loop until all the "scan lines" have reached heightmap bounds OR offset boundary OR coastline boundary.
                            while (continueIncrementPositiveY || continueIncrementNegativeY || continueIncrementPositiveX || continueIncrementNegativeX)
                            {

                                // Generate "scan line" to positive y-direction from the curIdx
                                if (continueIncrementPositiveY)
                                {
                                    scanlinePositiveY.Y++;  // Increment

                                    // Distance between the recent map block generation coordinate (in world coordinates) and the scanline.
                                    squareDist = Mathf.Pow(scanlinePositiveY.X - mapBlockGenerationWorldCoordinateIdx.X, 2) +
                                        Mathf.Pow(scanlinePositiveY.Y - mapBlockGenerationWorldCoordinateIdx.Y, 2);

                                    // Stop scanning to positive y-direction IF we 
                                    //   - are out of the offset boundary
                                    //   - are out of the heightmap bounds (index)
                                    //   - have reached the coastline boundary.
                                    if ((squareDist > Mathf.Pow(offsetInHeightmapIdx, 2)) ||
                                        (scanlinePositiveY.Y > resolution - 1) ||
                                        (coastlineMap[scanlinePositiveY.Y, scanlinePositiveY.X] == true))
                                    {
                                        continueIncrementPositiveY = false;
                                    }
                                    // Else continue scanning and associate the current heightmap index as water.
                                    else
                                    {
                                        heightmap[scanlinePositiveY.Y, scanlinePositiveY.X] = 0.0f;
                                    }
                                }

                                // Generate "scan line" to negative y-direction from the curIdx
                                if (continueIncrementNegativeY)
                                {
                                    scanlineNegativeY.Y--;

                                    squareDist = Mathf.Pow(scanlineNegativeY.X - mapBlockGenerationWorldCoordinateIdx.X, 2) +
                                        Mathf.Pow(scanlineNegativeY.Y - mapBlockGenerationWorldCoordinateIdx.Y, 2);

                                    if ((squareDist > Mathf.Pow(offsetInHeightmapIdx, 2)) ||
                                        (scanlineNegativeY.Y < 0) ||
                                        (coastlineMap[scanlineNegativeY.Y, scanlineNegativeY.X] == true))
                                    {
                                        continueIncrementNegativeY = false;
                                    }
                                    else
                                    {
                                        heightmap[scanlineNegativeY.Y, scanlineNegativeY.X] = 0.0f;
                                    }
                                }

                                // Generate "scan line" to positive x-direction from the curIdx
                                if (continueIncrementPositiveX)
                                {
                                    scanlinePositiveX.X++;

                                    squareDist = Mathf.Pow(scanlinePositiveX.X - mapBlockGenerationWorldCoordinateIdx.X, 2) +
                                        Mathf.Pow(scanlinePositiveX.Y - mapBlockGenerationWorldCoordinateIdx.Y, 2);

                                    if ((squareDist > Mathf.Pow(offsetInHeightmapIdx, 2)) ||
                                        (scanlinePositiveX.X > resolution - 1) ||
                                        (coastlineMap[scanlinePositiveX.Y, scanlinePositiveX.X] == true))
                                    {
                                        continueIncrementPositiveX = false;
                                    }
                                    else
                                    {
                                        heightmap[scanlinePositiveX.Y, scanlinePositiveX.X] = 0.0f;
                                    }
                                }

                                // Generate "scan line" to negative x-direction from the curIdx
                                if (continueIncrementNegativeX)
                                {
                                    scanlineNegativeX.X--;

                                    squareDist = Mathf.Pow(scanlineNegativeX.X - mapBlockGenerationWorldCoordinateIdx.X, 2) +
                                        Mathf.Pow(scanlineNegativeX.Y - mapBlockGenerationWorldCoordinateIdx.Y, 2);

                                    if ((squareDist > Mathf.Pow(offsetInHeightmapIdx, 2)) ||
                                        (scanlineNegativeX.X < 0) ||
                                        (coastlineMap[scanlineNegativeX.Y, scanlineNegativeX.X] == true))
                                    {
                                        continueIncrementNegativeX = false;
                                    }
                                    else
                                    {
                                        heightmap[scanlineNegativeX.Y, scanlineNegativeX.X] = 0.0f;
                                    }
                                }

                            }


                            // Increment curIdx
                            if (direction == 0)     // Positive x
                            {
                                curIdx.X++;
                            }
                            if (direction == 1)     // Negative x
                            {
                                curIdx.X--;
                            }
                            if (direction == 2)     // Positive y
                            {
                                curIdx.Y++;
                            }
                            if (direction == 3)     // Negative y
                            {
                                curIdx.Y--;
                            }
                        }
                    }
                }
            }
        }

        ApplyPolygonEdgeSmoothingWindow(ref heightmap, indices);    // Smooth polygon edges using NxN window
    }
    // * * * * * * * * END OF CreateCoastlines() * * * * * * * * * 



    /*
     * Method creates a "groove" for the river and smooths edges of the groove.
     */
    private void ApplyRiverWindow(ref float[,] heightmap, List<AlphamapIndices> indices, int riverWidth, float riverEdgeSlope, float riverEdgeRnd)
    {
        int slopeWidth = Mathf.CeilToInt(1 / riverEdgeSlope);
        int windowSize = riverWidth + slopeWidth * 2;
        int radius = (windowSize - 1) / 2;

        foreach (AlphamapIndices alphamapIndices in indices)    // Loop indices belonging to edges of the polygon
        {
            foreach (Idx idx in alphamapIndices.GetIndices())
            {
                // First create "groove" for the river by setting heightmap heights to 0.0
                for (int x = -radius; x <= radius; x++)     // Loop through the window
                {
                    for (int y = -radius; y <= radius; y++)
                    {
                        float distToWindowCenter = Mathf.Sqrt(x * x + y * y);

                        // Create "groove" by applying a window
                        if (distToWindowCenter <= (radius - slopeWidth))
                        {
                            heightmap[idx.Y + y, idx.X + x] = 0.0f;
                        }
                    }
                }

                // Then smooth edges of the "groove"
                for (int x = -radius; x <= radius; x++)     // Loop through the window
                {
                    for (int y = -radius; y <= radius; y++)
                    {
                        float distToWindowCenter = Mathf.Sqrt(x * x + y * y);
                        float curValue = heightmap[idx.Y + y, idx.X + x];

                        if(curValue > 0.01)    // Don't apply smoothing to the bottom of the "groove"
                        {
                            if ((distToWindowCenter <= radius) && (distToWindowCenter > (radius - slopeWidth)))  // Consider only edges of the "groove"
                            {
                                float rnd = Random.Range(-riverEdgeRnd, riverEdgeRnd);
                                heightmap[idx.Y + y, idx.X + x] =
                                    curValue - (riverEdgeSlope * Mathf.Clamp(radius - distToWindowCenter - rnd, 0.0f, radius) / terrainMaxHeight);
                            }
                        }

                    }
                }
            }
        }
    }
    // * * * * * * * * END OF ApplyRiverWindow() * * * * * * * * * 



    /*
     * Method smooths linearly lake and riverbank edges
     */
    private void ApplyPolygonEdgeSmoothingWindow(ref float[,] heightmap, List<AlphamapIndices> indices)
    {
        int radius = (waterPolygonSmoothingWindowSize - 1) / 2;

        foreach (AlphamapIndices alphamapIndices in indices)    // Loop indices belonging to edges of the polygon
        {
            foreach (Idx idx in alphamapIndices.GetIndices())
            {

                // Apply smoothing window for polygon edges
                for (int x = -radius; x <= radius; x++)     // Loop through the window
                {
                    for (int y = -radius; y <= radius; y++)
                    {
                        float curValue = heightmap[idx.Y + y, idx.X + x];

                        if (curValue > 0.01)    // Useless to process bottom of the lake
                        {
                            if (x == 0 && y == 0)    // Raise up the middle pixel of the window
                            {
                                heightmap[idx.Y + y, idx.X + x] = terrainBaseHeight / terrainMaxHeight;
                            }

                            // Smooth and add some randomness
                            float distToWindowCenter = Mathf.Sqrt(x * x + y * y);
                            float rnd = Random.Range(-waterPolygonEdgeRnd, waterPolygonEdgeRnd);
                            heightmap[idx.Y + y, idx.X + x] =
                                curValue - (waterPolygonSmoothingSlope * Mathf.Clamp(radius - distToWindowCenter - rnd, 0.0f, radius) / terrainMaxHeight);
                        }
                    }
                }
            }
        }
    }
    // * * * * * * * * END OF ApplyPolygonEdgeSmoothingWindow() * * * * * * * * * 


}
