﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class LanduseTexturizer : BaseTexturizer<RasterGeometry>
{

    private Rasterizer rasterizer;

    // Constructor
    public LanduseTexturizer(TerrainData terrainData, int numLayers, Dictionary<string, Coord> worldTransformedBbox) 
        : base(terrainData, numLayers)
    {
        rasterizer = new Rasterizer(terrainData.size.x, terrainData.size.z, terrainData.alphamapResolution, worldTransformedBbox);
    }

    public override float[,,] GenerateAlphamap(List<RasterGeometry> rasterizedGeometries)
    {
        foreach (RasterGeometry rasterizedPolyg in rasterizedGeometries)
        {
            AlphamapIndices polygonIndices = rasterizedPolyg.GetRaster()[0];

            foreach (Idx idx in polygonIndices.GetIndices() )
            {
                alphamapLayers[idx.Y, idx.X, 0] = 0.7f;
            }
        }

        return alphamapLayers;
    }

}
