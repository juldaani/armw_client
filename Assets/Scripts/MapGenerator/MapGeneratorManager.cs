﻿/* 
 * MapGeneratorManager is central a "auhtority" which manages the map creation. 
 * It checks player's position on regular time intervals and updates the map if the player has moved
 * more than a certain distance threshold away from the last update location. 
 */

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MapGeneratorManager : MonoBehaviour
{
    private static readonly string vectorApiUrl = "http://51.254.204.150/vector_api/";

    // Request parameters
    // For example, Rauma: lon=21.525356, lat = 61.130697
    public float longitude;
    public float latitude;
    public int offset;
    public int resolution;          // Height and alphamap resolution (should be 2^n + 1 )
    public int padding;             // Padding around the map block
    public List<string> tags;
    private bool isLocationServiceInitialized;

    public float gpsAccuracyInMetres;
    public float gpsUpdateDistInMetres;

    public int updateDistance;
    public float updateInterval;    // in seconds
    private bool isGeneratingMap;

    public bool isMobile;
    public bool isUnityRemote;

    Coord mapBlockGenerationCoordinateLonLat;     // Coordinates where map data was loaded from the server 
    Coord mapBlockGenerationCoordinateXY;         // Same but in web mercator projection

    private VectorGeometries geometries;

    GameObject deviceLocationObj;
    GameObject playerObj;

    Vector3 deviceLocationWorld;

    VectorDataRequester vectorDataRequester;
    TerrainGenerator terrainGenerator;
    BuildingGenerator buildingGenerator;
    PointObjectGeneratorTest pointObjectGenerator;

    RasterGeometries rasterGeometries;


    // Draw debug circle around the update distance
    void OnDrawGizmos()
    {
        int terrainBaseHeight = GetComponent<TerrainGenerator>().terrainBaseHeight;
        float terrainMaxHeight = GetComponent<TerrainGenerator>().height;

        if ((geometries != null) && (geometries.GetBbox()["min"] != null))
        {
            Coord mapBlockWorldCoord = CoordinateConverter.WebMercatorToWorldCoordinate(mapBlockGenerationCoordinateXY, geometries);
            Vector3 mapBlockGenerationWorldCoordinate = new Vector3(mapBlockWorldCoord.X, terrainMaxHeight-terrainMaxHeight*0.75f, mapBlockWorldCoord.Y);

            Gizmos.color = Color.yellow;
            //Gizmos.DrawSphere(new Vector3(0,0,0), 50);
            Gizmos.DrawWireSphere(mapBlockGenerationWorldCoordinate, updateDistance);
        }
        
    }


    void Start()
    {
        deviceLocationObj = GameObject.FindGameObjectWithTag("DeviceLocation");
        playerObj = GameObject.FindGameObjectWithTag("Player");

        vectorDataRequester = gameObject.GetComponent<VectorDataRequester>();
        terrainGenerator = gameObject.GetComponent<TerrainGenerator>();
        buildingGenerator = gameObject.GetComponent<BuildingGenerator>();
        pointObjectGenerator = gameObject.GetComponent<PointObjectGeneratorTest>();
        rasterGeometries = gameObject.GetComponent<RasterGeometries>();


        if (isMobile)
        {
            isLocationServiceInitialized = false;
            Debug.Log("******* Start location service ******");

            StartCoroutine(StartLocationService(gpsAccuracyInMetres, gpsUpdateDistInMetres) );
        }

        if (!isMobile)
        {
            mapBlockGenerationCoordinateLonLat = new Coord(longitude, latitude);
            mapBlockGenerationCoordinateXY = CoordinateConverter.ToWebMercator(longitude, latitude);
            StartCoroutine(CreateMap() );
        }

        // Check players location every n seconds and update the map if desired
        InvokeRepeating("UpdateMap", 0.0f, updateInterval);
    }


    /*
     * Start location service of a mobile device
     */
    private IEnumerator StartLocationService(float desiredAccuracyInMeters, float updateDistanceInMeters)
    {
        // Wait until the editor and unity remote are connected before starting a location service
        if (isUnityRemote)
        {
            yield return new WaitForSeconds(2);
        }

        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
        {
            Debug.Log("No locations enabled in the device");
            yield break;
        }

        // Start service before querying location
        Input.location.Start();

        if (isUnityRemote)
        {
            yield return new WaitForSeconds(2);
        }

        // Wait until service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            Debug.Log("Service didn't initialize in 20 seconds");
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            Debug.Log("Unable to determine device location");
            yield break;
        }
        else    // Access granted and location value could be retrieved
        {
            isLocationServiceInitialized = true;
            float lat = Input.location.lastData.latitude;
            float lon = Input.location.lastData.longitude;
            // !!!!!!!!!!!!!! CreateMap must be fixed to take lat and lon variables !!!!!
            StartCoroutine(CreateMap() );   
        }
    }
    // * * * * * * * * * END OF StartLocationService() * * * * * * * * * 


    /*
     * Get player location from gps and transform it to web mercator projection
     */
    private Coord GetPlayerLocationLonLat()
    {
        Coord playerLocationLonLat = null;

        if (isLocationServiceInitialized)
        {
            // If location service connection has failed
            if (Input.location.status == LocationServiceStatus.Failed)
            {
                Debug.Log("Unable to determine device location");
            }
            else
            {
                float lat = Input.location.lastData.latitude;
                float lon = Input.location.lastData.longitude;

                playerLocationLonLat = new Coord(lon, lat);
            }
        }

        return playerLocationLonLat;
    }


    void FixedUpdate()
    {
        if (isMobile && isLocationServiceInitialized)
        {
            // Smooth device location movement
            Vector3 diff = deviceLocationWorld - deviceLocationObj.transform.position;
            float speed = 0.005f;

            if (diff.sqrMagnitude > 1500)
            {
                speed = 0.01f;
            }

            Vector3 moveTo = diff * speed;
            deviceLocationObj.transform.Translate(moveTo);
        }

    }


    /* 
     * If the player has moved certain distance away from the last location where the map was updated 
     * then update the map. 
     * New map is generated into the origo and player's position is transformed to location in which a new map block was
     * requested.
     */
    void UpdateMap()
    {
        if (geometries != null)     // If the map block is not empty
        {

            //  * * * * * * * If app is running on mobile using gps * * * * * * * *
            // 
            //  !!! Mobile possibly not working properly (not tested yet) after the fix in which the map block generation 
            //      coordinate was moved to origo. !!!!
            //
            if (isMobile && isLocationServiceInitialized)
            {
                Coord deviceLocationLonLat = GetPlayerLocationLonLat();
                Coord deviceLocationWebMercator = CoordinateConverter.ToWebMercator(deviceLocationLonLat.X, deviceLocationLonLat.Y);

                if (deviceLocationLonLat != null)
                {
                    // Transform device location to world coordinates (Device Location game object is moved in FixedUpdate() )
                    Coord deviceWorldCoord = CoordinateConverter.WebMercatorToWorldCoordinate(deviceLocationWebMercator, geometries);
                    deviceLocationWorld = new Vector3(deviceWorldCoord.X, deviceLocationObj.transform.position.y, deviceWorldCoord.Y);

                    // Get coordinates (world coordinates) where the map block was generated
                    Coord mapBlockWorldCoord = CoordinateConverter.WebMercatorToWorldCoordinate(mapBlockGenerationCoordinateXY, geometries);
                    Vector3 mapBlockGenerationWorldCoordinate = new Vector3(mapBlockWorldCoord.X , 0, mapBlockWorldCoord.Y);    // Just convert to Vector3

                    // Calculate distance travelled from the point where the map block was generated
                    Vector3 tmpPlayerLocation = new Vector3(deviceLocationWorld.x, 0, deviceLocationWorld.z);
                    float dist = Vector3.Distance(mapBlockGenerationWorldCoordinate, tmpPlayerLocation);

                    // If not currently generating new map block and if the player has travelled far enough
                    if (!isGeneratingMap && (dist > updateDistance))
                    {
                        isGeneratingMap = true;

                        float lon = deviceLocationLonLat.X;
                        float lat = deviceLocationLonLat.Y;

                        StartCoroutine(CreateMap() );
                    }

                }
            }


            // * * * * * * * * * If the app is running on pc * * * * * * * * * * * *
            if (!isMobile)
            {
                Vector3 mapBlockGenerationWorldCoordinate = new Vector3(0, 0, 0);   // A new map block is generated to origo

                Vector3 deviceLocationWorld = deviceLocationObj.transform.position;     // Device location

                // Calculate distance travelled from the point where the map block was generated
                Vector3 tmpDeviceLocation = new Vector3(deviceLocationWorld.x, 0, deviceLocationWorld.z);   // Ignore height (y-coordinate)
                float dist = Vector3.Distance(mapBlockGenerationWorldCoordinate, tmpDeviceLocation);

                // If not currently generating new map block and if the player has travelled far enough
                if (!isGeneratingMap && (dist > updateDistance))
                {
                    isGeneratingMap = true;

                    StartCoroutine(CreateMap() );
                }
            }
        }
    }
    // * * * * * * * * * END OF UpdateMap() * * * * * * * * * 


    /*
     * Create new map based on the data downloaded from the server
     */
    private IEnumerator CreateMap()
    {
        // Calculate geographic and web mercator coordinates from the device's current location in the world
        Vector3 deviceLocationWorld = deviceLocationObj.transform.position;     // Device location
        Coord deviceLocationWebMercator = new Coord(mapBlockGenerationCoordinateXY.X + deviceLocationWorld.x,
            mapBlockGenerationCoordinateXY.Y + deviceLocationWorld.z);
        Coord deviceLocationLonLat = CoordinateConverter.ToGeographic(deviceLocationWebMercator.X,
            deviceLocationWebMercator.Y); 

        // Fetch map data from the server according to lon and lat coordinates
        float lon = deviceLocationLonLat.X;
        float lat = deviceLocationLonLat.Y;
        vectorDataRequester.SendRequestToServer(lon, lat, offset, tags, vectorApiUrl);
        yield return StartCoroutine(WaitUntilJsonDataIsDownloaded());   // Wait until download is finished
        WWW geoJsonData = vectorDataRequester.GetGeoJsonData();
        geometries = new VectorGeometries(geoJsonData.text, offset, lon, lat);    // Parse to geometry objects (point, polygon, linestring)

        // Rasterize geometries
        float worldLengthX = offset * 2 + padding * 2;
        float worldLengthZ = offset * 2 + padding * 2;
        rasterGeometries.RasterizeGeometries(geometries, worldLengthX, worldLengthZ, resolution);

        // Coordinates where the current map block was generated
        mapBlockGenerationCoordinateLonLat = deviceLocationLonLat;
        mapBlockGenerationCoordinateXY = deviceLocationWebMercator;
        Coord mapBlockGenerationCoordinateWorld = new Coord(0, 0);      // A new map block is created to origo
        
        // Generate terrain
        Terrain terrain = terrainGenerator.GenerateTerrain(geometries, offset, mapBlockGenerationCoordinateWorld);

        // Generate point objects
        try
        {
            pointObjectGenerator.GenerateBusStops(geometries);
        }
        catch (KeyNotFoundException e)
        {
            Debug.Log(e);
        }

        // Generate buildings
        try
        {
            buildingGenerator.GenerateBuildings(geometries, terrain);
        }
        catch(System.NullReferenceException e)
        {
            Debug.Log(e);
        }

        // Adjust device initial location in a new map block (decive can move during the data downloading and terrain generation..)
        Vector3 currentDeviceLocation = deviceLocationObj.transform.position;
        Vector3 diffDevice = currentDeviceLocation - deviceLocationWorld;
        deviceLocationObj.transform.position = new Vector3(diffDevice.x, currentDeviceLocation.y, diffDevice.z);

        // !!!!!!!! This is not working properly, must be rewritten !!!!!!
        // Adjust player initial location in a new map block (player can move during the data downloading and terrain generation..)
        Vector3 diffPlayer = playerObj.transform.position - deviceLocationWorld;
        playerObj.transform.position = new Vector3(diffPlayer.x, deviceLocationWorld.y, diffPlayer.z);
        // !!!!!!!!!!!

        Debug.Log("Map generator has loaded geometry data succesfully.");
        isGeneratingMap = false;
    }
    // * * * * * * * * * END OF CreateMap() * * * * * * * * * 


    /*
     * Waits until the map data is downloaded from the server
     */
    private IEnumerator WaitUntilJsonDataIsDownloaded()
    {
        bool isDownloadFinished = vectorDataRequester.IsDownloadFinsished();

        while (!isDownloadFinished)     // Wait until the download is done
        {
            isDownloadFinished = vectorDataRequester.IsDownloadFinsished();
            yield return null;
        }
    }


    // Getters & setters
    public VectorGeometries GetGeometries()
    {
        return geometries;
    }
    
    public int GetOffset()
    {
        return offset;
    }

    public int GetResolution()
    {
        return resolution;
    }

    public int GetPadding()
    {
        return padding;
    }
}
